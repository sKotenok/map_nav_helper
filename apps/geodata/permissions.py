

from common.permission import BasePermission, is_user_root



class BuildingPermission(BasePermission):

	def can_add(self, instance):
		"""Only root can add new building!."""
		return is_user_root(self.user)


class FloorPermission(BasePermission):

	def can_add(self, instance):
		"""User can add floor only for his organization's buildings"""
		if is_user_root(self.user):
			return True
		elif self.user.organization_id:
			building = instance.building.get_or_404()
			return building.organization_id == self.user.organization_id
		return False


class GeoObjectPermission(BasePermission):

	def can_add(self, instance):
		"""User can add geo object only for his own orgnaization's building"""
		if is_user_root(self.user):
			return True
		elif self.user.organization_id:
			building = instance.building.get_or_404()
			return building.organization_id == self.user.organization_id
		return False


