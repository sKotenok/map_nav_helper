"""Add Some Foreign Keys

Revision ID: 2684c6374164
Revises: 0bb6f6dda0f4
Create Date: 2018-10-16 10:13:12.627112

manage.py db revision --autogenerate -m "Add Some Foreign Keys"
"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '2684c6374164'
down_revision = '0bb6f6dda0f4'
branch_labels = None
depends_on = None


def upgrade():
	op.add_column('building', sa.Column('user_id', sa.Integer(), nullable=False))
	op.create_foreign_key(None, 'building', 'user', ['user_id'], ['id'])

	op.add_column('floor', sa.Column('organization_id', sa.Integer(), nullable=False))
	op.add_column('floor', sa.Column('user_id', sa.Integer(), nullable=False))
	op.create_foreign_key(None, 'floor', 'user', ['user_id'], ['id'])
	op.create_foreign_key(None, 'floor', 'organization', ['organization_id'], ['id'])

	op.add_column('geo_object', sa.Column('user_id', sa.Integer(), nullable=False))
	op.create_foreign_key(None, 'geo_object', 'user', ['user_id'], ['id'])


def downgrade():
	op.drop_column('geo_object_type', 'geo_type')

	op.drop_constraint(None, 'geo_object', type_='foreignkey')
	op.drop_column('geo_object', 'user_id')

	op.drop_constraint(None, 'floor', type_='foreignkey')
	op.drop_constraint(None, 'floor', type_='foreignkey')
	op.drop_column('floor', 'user_id')
	op.drop_column('floor', 'organization_id')

	op.drop_constraint(None, 'building', type_='foreignkey')
	op.drop_column('building', 'user_id')
