from extensions import db
from models import auth, building, geodata

# Auth
o0 = auth.Organization(id=1, name='Галлери-Мобайл')
organizations = [ o0, auth.Organization(id=2, name='Ещё какая-то фирма'), ]

roles = [
	auth.Role(id=1, name='root', organization=o0),
	auth.Role(id=2, name='manager', organization=o0),
]

u0 = auth.User(id=1, name='root', email='root@localhost', password='root00', organization=o0, roles=[roles[0]])
u1 = auth.User(id=2, name='admin', email='admin@localhost', password='12345', organization=o0, roles=[roles[0]])
users = [ u0, u1 ]

# Building
cn0 = building.Country(id=1, name='Российская Федерация')
countries = [ cn0, ]

ct0 = building.City(id=2, name='Набережные Челны', country=cn0)
cities = [
	building.City(id=1, name='Москва', country=cn0),
	ct0,
]

b0 = building.Building(
	id=1,
	name='рынок Алан',
	coords=[52.41224, 55.765427],
	geodata="""[[
[52.410940825939164, 55.76568744014449],
[52.41220146417617, 55.764948045986415],
[52.41318315267562, 55.76547015025497],
[52.413827050477266, 55.76593519063735],
[52.413302594795816, 55.76624306110597],
[52.412410005927086, 55.76573006805367],
[52.41173677146435, 55.76612465692946],
[52.410940825939164, 55.76568744014449]
]]""",
	address='ул. Чулман, рынок',
	short_description='Всякий дешёвый ширпотреб втридорога',
	entries="""[
[52.410, 55.766],
[52.413250, 55.766239],
[52.413250, 55.766239]
]""",
	city=ct0,
	organization=o0,
	user=u0
)
buildings = [ b0, ]

f0 = building.Floor(
	id=1,
	building=b0,
	name='1й этаж',
	position=1,
	user=u0,
	organization=o0
)
floors = [ f0, ]


gt0 = geodata.GeoObjectType(id=1, name='Bluetooth beacon', geo_type='point')
geotypes = [
	gt0,
	geodata.GeoObjectType(id=2, name='WiFi point', geo_type='point'),
]

gp0 = geodata.GeoProperty(id=1, name='mac_address', type='string')
geo_properties = [ gp0, ]


go0 = geodata.GeoObject(
	id=1,
	coords=[52.41224, 55.765427],
	type=gt0,
	floor=f0,
	building=b0,
	organization=o0,
	user=u0,
)
geoobjects = [
	go0,
]

geo_values = [
	geodata.GeoPropertyValue(id=1, value='11:22:33', geo_property=gp0, geo_object=go0)
]

def add_all():
	for rows in (organizations, roles, users,
			cities, countries, buildings, floors,
			geotypes, geo_properties, geoobjects, geo_values):
		for row in rows:
			print('ROW:', row)
			db.session.add(row)
	db.session.commit()




