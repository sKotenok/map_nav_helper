from extensions import db
from common.base_model import OwnerMixin


class GeoObjectType(db.Model, OwnerMixin):
	"""GeoObject model"""
	__tablename__ = 'geo_object_type'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100), nullable=False)
	geo_type = db.Column(db.String(100), nullable=False, default='point')

	def __repr__(self):
		return "<GeoObjectType %d %s>" % (self.id, self.name)

	# У общеих справочников нет владельца, поэтому изменить их может только root
	def get_owner_organization(self): return None
	def get_owner_user(self): return None


class GeoObject(db.Model, OwnerMixin):
	"""LayerData model"""
	__tablename__ = 'geo_object'
	id = db.Column(db.Integer, primary_key=True)
	geodata = db.Column(db.Text, nullable=True)

	# Координаты центра здания
	coords_lat = db.Column(db.Numeric(16, 9, asdecimal=False), default=0.0, nullable=False)
	coords_lng = db.Column(db.Numeric(16, 9, asdecimal=False), default=0.0, nullable=False)

	type_id = db.Column(db.Integer, db.ForeignKey('geo_object_type.id'), nullable=False)
	type = db.relationship('GeoObjectType', backref='geo_objects')

	floor_id = db.Column(db.Integer, db.ForeignKey('floor.id'), nullable=False)
	floor = db.relationship('Floor', backref=db.backref('layers', lazy='dynamic'))

	building_id = db.Column(db.Integer, db.ForeignKey('building.id'), nullable=True)
	building = db.relationship('Building', backref=db.backref('control_points', lazy='dynamic'))

	organization_id = db.Column(db.Integer, db.ForeignKey('organization.id'), nullable=True)
	organization = db.relationship('Organization', backref=db.backref('geo_objects', lazy='dynamic'))

	user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
	user = db.relationship('User', backref=db.backref('geo_objects', lazy='dynamic'))

	time_created = db.Column(db.TIMESTAMP(timezone=True), server_default=db.func.now(), nullable=False)
	time_updated = db.Column(db.TIMESTAMP(timezone=True), server_default=db.func.now(), onupdate=db.func.now(), nullable=False)

	def __repr__(self):
		return '<GeoObject {0} {1} {2}>'.format(self.id, self.floor_id, self.type_id)

	# --- COORDS dynamic property ---
	@property
	def coords(self): return (self.coords_lng, self.coords_lat)
	@coords.setter
	def coords(self, coords): self.coords_lng, self.coords_lat = coords

	def get_owner_organization(self): return self.organization_id
	def get_owner_user(self): return self.user_id


class GeoProperty(db.Model, OwnerMixin):
	__tablename__ = 'geo_property'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100), nullable=False, default='')
	type = db.Column(db.String(100), nullable=False, default='')

	# У общеих справочников нет владельца, поэтому изменить их может только root
	def get_owner_organization(self): return None
	def get_owner_user(self): return None


class GeoPropertyValue(db.Model, OwnerMixin):
	__tablename__ = 'geo_property_value'
	id = db.Column(db.Integer, primary_key=True)
	value = db.Column(db.Text(), nullable=True)

	geo_object_id = db.Column(db.Integer, db.ForeignKey('geo_object.id'), nullable=False)
	geo_object = db.relationship('GeoObject', backref=db.backref('properties', lazy='dynamic'))

	geo_property_id = db.Column(db.Integer, db.ForeignKey('geo_property.id'), nullable=False)
	geo_property = db.relationship('GeoProperty', backref=db.backref('values', lazy='dynamic'))

	def get_owner_organization(self): return self.geo_object.organization_id
	def get_owner_user(self): return self.geo_object.user_id

