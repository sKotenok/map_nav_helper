import os
from flask import request, render_template, Blueprint, abort, current_app, jsonify, url_for, redirect
from werkzeug.utils import secure_filename
from flask_jwt_extended import jwt_required, jwt_optional, get_current_user


blueprint = Blueprint('editor', __name__, url_prefix='/editor')


@blueprint.route('/index.html', methods=['GET'])
@blueprint.route('/', methods=['GET'])
@jwt_optional
def index():
	"""Just show an index page"""
	user = get_current_user()
	if not user:
		return redirect(url_for('auth.login_page'))
	return render_template('editor/index.html')





@blueprint.route('/upload-file', methods=['POST'])
@jwt_required
def upload_file(type='other', key=None):
	"""File uploading method"""
	type = request.values['type'] or type
	key = request.values['key'] or key

	print(request.files)

	if 'file' not in request.files:
		return abort(403)

	file = request.files['file']
	if file.filename == '':
		return abort(403)

	result_dict = create_file(file, type, key)
	return jsonify(result_dict)




def create_file(file, type, key):
	"""Create file name based on its type"""
	real_filename = ''
	upload_folder = ''
	result_dict = {}
	if type == 'bitmap':
		upload_folder = current_app.config['FILES_BITMAP_DIR']
		real_filename = secure_filename(file.filename)

		filepath = os.path.join(os.path.abspath('api/static/' + upload_folder), real_filename)

		print(filepath)

		file.save(filepath, 262144)
		file.close()
		#flash('Can not save file' + filepath)
		#print('Can not save file', filepath)
		#return abort(403)

		from PIL import Image
		with Image.open(filepath) as img:
			width, height = img.size

		result_dict = {
			'real_filename': real_filename,
			'results': {
				'url': url_for('static', filename=upload_folder + '/' + real_filename),
				'width': width,
				'height': height,
				'scale': float(key)
			},
		}
	return result_dict

