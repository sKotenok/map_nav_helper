from extensions import db



class Building(db.Model):
	"""Building model"""
	__tablename__ = 'building'
	id = db.Column(db.Integer, primary_key=True)
	active = db.Column(db.Boolean, default=True, nullable=False)

	city_id = db.Column(db.Integer, db.ForeignKey('city.id'), nullable=False)
	city = db.relationship('City', backref=db.backref('buildings', lazy='dynamic'))

	address = db.Column(db.String(300), default='', nullable=False)
	name = db.Column(db.String(80), nullable=False)
	desc = db.Column(db.Text, default='', nullable=False)

	geodata = db.Column(db.Text, nullable=True)

	coords_lat = db.Column(db.Numeric(16, 9, asdecimal=False), default=0.0, nullable=False)
	coords_lng = db.Column(db.Numeric(16, 9, asdecimal=False), default=0.0, nullable=False)
	coords_angle = db.Column(db.Float, default=0.0, nullable=False)

	time_created = db.Column(db.TIMESTAMP(timezone=True), server_default=db.func.now(), nullable=False)
	time_updated = db.Column(db.TIMESTAMP(timezone=True), server_default=db.func.now(), onupdate=db.func.now(), nullable=False)

	def __repr__(self):
		return "<Building {0} {1}>".format(self.id, self.name)


class Floor(db.Model):
	"""Floor model"""
	__tablename__ = 'floor'
	id = db.Column(db.Integer, primary_key=True)

	building_id = db.Column(db.ForeignKey('building.id'), nullable=False)
	building = db.relationship('Building', backref=db.backref('floors', lazy='dynamic'))

	name = db.Column(db.String(100), default='', nullable=False)
	position = db.Column(db.Integer(), default=0, nullable=False)

	geodata = db.Column(db.Text())

	bitmap_url = db.Column(db.String(1000))
	bitmap_width = db.Column(db.Integer(), default=0.0, nullable=False)
	bitmap_height = db.Column(db.Integer(), default=0.0, nullable=False)
	bitmap_scale = db.Column(db.Float(asdecimal=False), default=0.0, nullable=False)

	bitmap_simple_url = db.Column(db.String(1000), default='', nullable=False)
	bitmap_simple_width = db.Column(db.Integer(), default=0.0, nullable=False)
	bitmap_simple_height = db.Column(db.Integer(), default=0.0, nullable=False)
	bitmap_simple_scale = db.Column(db.Float(asdecimal=False), default=0.0, nullable=False)

	time_created = db.Column(db.TIMESTAMP(timezone=True), server_default=db.func.now(), nullable=False)
	time_updated = db.Column(db.TIMESTAMP(timezone=True), server_default=db.func.now(), onupdate=db.func.now(), nullable=False)

	def __repr__(self):
		return '<Floor {0} {1}>'.format(self.id, self.name)


class Layer(db.Model):
	"""LayerData model"""
	__tablename__ = 'layer'
	id = db.Column(db.Integer, primary_key=True)

	floor_id = db.Column(db.Integer, db.ForeignKey('floor.id'), nullable=False)
	floor = db.relationship('Floor', backref=db.backref('layers', lazy='dynamic'))

	type_id = db.Column(db.Integer, db.ForeignKey('layer_type.id'), nullable=False)
	type = db.relationship('LayerType', backref='layers')

	geodata = db.Column(db.Text, nullable=True)

	time_created = db.Column(db.TIMESTAMP(timezone=True), server_default=db.func.now(), nullable=False)
	time_updated = db.Column(db.TIMESTAMP(timezone=True), server_default=db.func.now(), onupdate=db.func.now(), nullable=False)

	def __repr__(self):
		return '<Layer {0} {1} {2}>'.format(self.id, self.floor_id, self.type_id)


class LayerType(db.Model):
	"""LayerType model"""
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100), nullable=False)

	def __repr__(self):
		return "<LayerType %d %s>" % (self.id, self.name)


class ControlPointType(db.Model):
	"""LayerType model"""
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100), nullable=False)

	def __repr__(self):
		return "<ControlPointType %d %s>" % (self.id, self.name)


class ControlPoint(db.Model):
	"""ControlPoint model"""
	id = db.Column(db.Integer, primary_key=True)
	sid = db.Column(db.String(16), unique=True)
	type_id = db.Column(db.Integer, db.ForeignKey('control_point_type.id'), nullable=False)
	type = db.relationship('ControlPointType', backref='control_points')

	building_id = db.Column(db.Integer, db.ForeignKey('building.id'), nullable=True)
	building = db.relationship('Building', backref=db.backref('control_points', lazy='dynamic'))

	floor_id = db.Column(db.Integer, db.ForeignKey('floor.id'), nullable=False)
	floor = db.relationship('Floor', backref=db.backref('control_points', lazy='dynamic'))

	mac = db.Column(db.String(20), nullable=False)

	coords_lat = db.Column(db.Numeric(16, 7, asdecimal=False), default=0.0, nullable=False)
	coords_lng = db.Column(db.Numeric(16, 7, asdecimal=False), default=0.0, nullable=False)

	time_created = db.Column(db.TIMESTAMP(timezone=True), server_default=db.func.now(), nullable=False)
	time_updated = db.Column(db.TIMESTAMP(timezone=True), server_default=db.func.now(), onupdate=db.func.now(), nullable=False)

	def __repr__(self):
		return "<ControlPoint %d %s>" % (self.id, self.mac)



