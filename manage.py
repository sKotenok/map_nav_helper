#!venv/bin/python3
import click
from flask.cli import FlaskGroup

from app import create_app


def create_api(info):
	return create_app(cli=True)


@click.group(cls=FlaskGroup, create_app=create_api)
def cli():
	"""Main entry point"""
	pass


@cli.command('change-password')
@click.argument('login')
@click.argument('new-password')
def change_password(login, new_password):
	from models.auth import User
	from extensions import db

	user = User.query.filter_by(login=login).one()
	if user:
		user.set_password(new_password)
		db.session.commit()
		click.echo('password was changed')
	else:
		click.echo('there is no user with login {0}'.format(login))


@cli.command('create-admin')
def create_admin():
	from models.auth import User, Organization
	from extensions import db
	click.echo("create user admin (admin@admin.local)")

	admin_org = Organization(name='Галлери-Мобайл')

	admin_user = User(
		name = 'admin',
		email = 'admin@mail.local',
		password = '12345',
		active = True,
		organization=admin_org
	)
	db.session.add(admin_org)
	db.session.add(admin_user)
	db.session.commit()


@cli.command('add_user')
@click.argument('login')
@click.argument('password')
@click.argument('organization')
def add_user(login, password='123', organization=None):
	from models.auth import User
	from extensions import db

	organization = int(organization) if organization else None

	user = User(login=login, password=password, organization_id=organization)
	db.session.add(user)
	db.session.commit()



@cli.command('add-test-data')
def add_test_data():
	from test_data import add_all
	add_all()


if __name__ == "__main__":
	cli()
