import os

from flask import url_for
from werkzeug.utils import secure_filename



def upload_file_json(json_data, path_to_save='uploads', file_field='file'):
	file_data = json_data.get(file_field, None)
	if not file_data: return None, None

	file_data = file_data.decode('base64')

	real_file_name = secure_filename(json_data.get('name', generate_unique_file_name()))

	upload_path = os.path.join(os.path.abspath('static'), path_to_save)
	file_path = os.path.join(upload_path, real_file_name)
	file_url = url_for('static', filename=path_to_save + '/' + real_file_name)

	with open(file_path, 'wb') as fp:
		fp.write(file_data)
	return real_file_name, file_path, file_url


def upload_file_post(files, path_to_save='uploads', file_field='file'):
	file = files.get(file_field)
	if not file: return None

	real_file_name = secure_filename(file.filename)

	upload_path = os.path.join(os.path.abspath('static'), path_to_save)
	file_path = os.path.join(upload_path, real_file_name)
	file_url = url_for('static', filename=path_to_save + '/' + real_file_name)

	print(file_path)

	file.save(file_path, 262144)
	file.close()

	return real_file_name, file_path, file_url


def generate_unique_file_name():
	return '123.txt'


def get_image_props(resource_model, file_name, file_path, file_url):
	from PIL import Image
	with Image.open(file_path) as img:
		width, height = img.size

	image_props = {
		'name': file_name,
		'url': file_url,
		'width': width,
		'height': height,
	}
	return image_props



def plan_tile_generation(resource_model, file_name, file_path, file_url, zoom_levels=(16,17,18,19,20)):
	return {}


