from api.extensions import db
from api.common.tools import BaseEnum





class SenderType(BaseEnum):
	DISPATCHER = 1
	USER = 2
	GROUP_OWNER = 3

	@classmethod
	def get_default(cls):
		return cls.USER


class ReceiversType(BaseEnum):
	DISPATCHER = 1
	BUILDING = 2
	USERS = 3
	GROUP = 4
	ORGANIZATION = 5

	@classmethod
	def get_default(cls):
		return cls.DISPATCHER


class Message(db.Model):
	__tablename__ = 'message'
	id = db.Column(db.Integer, primary_key=True)

	building_id = db.Column(db.Integer, db.ForeignKey('building.id'), nullable=True)
	building = db.relationship('Building', backref=db.backref('messages', lazy='dynamic'))

	floor_id = db.Column(db.Integer, db.ForeignKey('floor.id'), nullable=True)
	floor = db.relationship('Floor', backref=db.backref('messages', lazy='dynamic'))

	active = db.Column(db.Boolean(), nullable=False, default=True)

	user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
	created_by = db.relationship('User', backref=db.backref('messages', lazy='dynamic'))

	time_created = db.Column(db.TIMESTAMP(timezone=True), server_default=db.func.now(), nullable=False)
	time_updated = db.Column(db.TIMESTAMP(timezone=True), server_default=db.func.now(), onupdate=db.func.now(), nullable=False)
	time_accepted = db.Column(db.TIMESTAMP(timezone=True), server_default=None, nullable=True)

	sender_type = db.Column(db.Enum(SenderType), nullable=False, default=SenderType.get_default())
	receivers_type = db.Column(db.Enum(ReceiversType), nullable=False, default=ReceiversType.get_default())

	receivers_count_all = db.Column(db.Integer(), nullable=False, default=0)
	receivers_count_accepted = db.Column(db.Integer(), nullable=False, default=0)

	coords_lat = db.Column(db.Numeric(16, 7, asdecimal=False), default=0.0, nullable=False)
	coords_lng = db.Column(db.Numeric(16, 7, asdecimal=False), default=0.0, nullable=False)

	text = db.Column(db.Text, default='', nullable=False)




class MessageAttachment(db.Model):
	__tablename__ = 'message_attachment'
	id = db.Column(db.Integer, primary_key=True)

	message_id = db.Column(db.Integer, db.ForeignKey('message.id'), nullable=False)
	message = db.relationship('Message', backref=db.backref('attachments', lazy='dynamic'))

	name = db.Column(db.String(300), nullable=True)
	data_type = db.Column(db.String(100), nullable=False, default='image/jpeg')
	data_href = db.Column(db.String(1000), default='')
	data = db.Column(db.Binary(), nullable=True)


# class MessageReceiverUnread(db.Model):
# 	__tablename__ = 'message_receiver_unread'
# 	id = db.Column(db.BigInteger, primary_key=True)
#
# 	message_id = db.Column(db.Integer, db.ForeignKey('message.id'), nullable=False)
# 	message = db.relationship('Message', backref=db.backref('receivers_unread', lazy='dynamic'))
#
# 	user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
# 	receiver = db.relationship('User', backref=db.backref('messages_unread', lazy='dynamic'))


class MessageReceiver(db.Model):
	__tablename__ = 'message_receiver'
	id = db.Column(db.BigInteger, primary_key=True)
	is_readed = db.Column(db.Boolean(), default=False, nullable=False)

	message_id = db.Column(db.Integer, db.ForeignKey('message.id'), nullable=False)
	message = db.relationship('Message', backref=db.backref('receivers', lazy='dynamic'))

	user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
	receiver = db.relationship('User', backref=db.backref('messages_received', lazy='dynamic'))


