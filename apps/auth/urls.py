from flask import request, jsonify, Blueprint, render_template, redirect, url_for, abort
from flask import Blueprint
from flask_jwt_extended import (
	create_access_token,
	create_refresh_token,
	jwt_refresh_token_required,
	get_jwt_identity,
	set_access_cookies,
	set_refresh_cookies,
	unset_jwt_cookies,
	jwt_required,
	jwt_optional,
	get_current_user,
	get_raw_jwt
)

from models.auth import User
from extensions import db, pwd_context, jwt
from .permissions import Permission


blueprint = Blueprint('auth', __name__, url_prefix='/auth')


@blueprint.route('/reset-password', methods=['POST'])
@jwt_required
def reset_password():
	return {}


@blueprint.route('/login', methods=['GET', 'POST'])
@jwt_optional
def login():
	"""
	Authenticate user and return token
	"""
	if request.method == 'POST':
		username = request.values.get('username')
		password = request.values.get('password')

		if not username or not password:
			return abort(403)

		user = User.query.filter_by(name=username).first()
		if user is None or not pwd_context.verify(password, user.password):
			return abort(403)

		access_token = create_access_token(identity=user.id)
		refresh_token = create_refresh_token(identity=user.id)


		resp = redirect(url_for('main_site.index'))
		set_access_cookies(resp, access_token)
		set_refresh_cookies(resp, refresh_token)
		return resp

	user = get_current_user()
	params = {
		'username': user.login if user else '',
		'password': ''
	}

	return render_template('auth/login.html', **params), 200





@blueprint.route('/logout', methods=['GET', 'POST'])
@jwt_required
def logout():
	resp = redirect(url_for('main_site.index'))
	unset_jwt_cookies(resp)
	return resp, 200


@blueprint.route('/me', methods=['GET','POST'])
@jwt_required
def me():
	user = get_current_user()
	return render_template('auth/me.html',user=user), 200


@blueprint.route('/users/')
@jwt_required
def users():
	user = get_current_user()
	users = get_allowed_users(user)
	return render_template('auth/user_list.html', user=user, users=users), 200


@blueprint.route('/user/<int:id>', methods=['GET'])
@jwt_required
def user(id=None):
	user_current = get_current_user()
	user_target = get_allowed_user(user_current, int(id))
	return render_template('auth/user_detail.html', user_current=user_current, user=user_target)


def get_allowed_users(user):
	# Вытаскивает список профилей пользователей, которых разрешено просматривать текущему.
	return []


def get_allowed_user(current, user_id):
	# Проверяет доступ текущего пользователя на просмотр к профилю user_id и возвращает его.
	return None













