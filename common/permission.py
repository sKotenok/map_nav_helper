from models.auth import is_user_root
from extensions import db
import collections

GLOBAL_USE_CACHE = False    # TODO Move to config

class CacheInstance:
	"""Обьект кэша для запросов из таблицы"""
	pass



class BasePermission:
	USE_CACHE = GLOBAL_USE_CACHE

	def __init__(self, user, model=None, cache_instance=None):
		self.user = user
		self.model = model
		self.cache = cache_instance() if cache_instance else CacheInstance()


	# Methods to override

	def can_read_table(self):
		"""Check if choosen user has read access for given model.
		Defaults: any authorized user can read."""
		return True

	def can_read(self, instance):
		"""Check if choosen user can read exact model instance.
		Defaults: any authorized user can read."""
		return True

	def filter_for_read(self, query):
		"""Filter given query for fields that can be readen by choosen user.
		Defaults: user can read any fields"""
		return query

	def can_add_table(self):
		"""Check if choosen user can add into given table.
		Defaults: any authorized user can add field to any table."""
		return True

	def can_add(self, model_instance):
		"""Check if choosen user can add exact field into given table.
		Defaults: any authorized user can add to any table."""
		return True

	def can_update(self, model_id):
		"""Check if choosen user can update given model instance.
		Defaults: any authorized user can update if he owns this instance or his organization owns it.
		root user can update anything."""
		if is_user_root(self.user):
			return True

		instance = self.model.query.get_or_404(model_id)

		owner_id = instance.get_owner_user_id()
		if owner_id and self.user.id == owner_id:
			return True

		organization_id = instance.get_owner_organization_id()
		if organization_id and self.user.organization_id == organization_id:
			return True
		return False

	def filter_for_update(self, query):
		"""Filter given query for fields that can be updated by choosen user.
		Defaults: any authorized user can update his own or his organization's intances only.
		root user can update anything."""
		if is_user_root(self.user):
			return query

		user_field = self.model.get_owner_user_field()
		organization_field = False
		if self.user.organization_id:
			organization_field = self.model.get_owner_organization_field()

		if user_field and organization_field:
			query = query.filter(db.or_(user_field == self.user.id, organization_field == self.user.organization_id))
		elif user_field:
			query = query.fitler(user_field == self.user.id)
		elif organization_field:
			query = query.filter(organization_field == self.user.organization_id)
		return query

	def can_delete(self, model_id):
		"""Check if choosen user can delete given model instance.
		By default: only root user can delete anything.
		"""
		if is_user_root(self.user):
			return True
		return False

	def __get_all_permissions(self):
		permissions = None
		if self.user:
			if self.user.organization_id:
				# Пользователь относится к одной из зареганых компаний.
				organization = self.user.organization
				buildings = organization.buildings
				if buildings:
					# Компания "владеет" хотя бы одним зданием.
					pass
				shops = organization.shops
				if shops:
					# Комания "владеет" хотя бы одним магазином.
					pass

			else:
				# Простой авторизованный пользователь.
				pass
		else:
			# Простой неавторизованный пользователь.
			pass
		return permissions




class DirectoryPermission(BasePermission):
	"""Anyone can read, only root can add or update"""
	def can_add_table(self): return is_user_root(self.user)

	def can_update(self, model_id): return is_user_root(self.user)

	def can_delete(self, model_id): return is_user_root(self.user)

	def filter_for_update(self, query):
		if is_user_root(self.user):
			return query
		else:
			return query.filter(db.raw('false'))

































	# def can(self, do, model_field=None):
	# 	if not self.user:
	# 		return False
	#
	# 	print('User:', self.user, 'DO:', do, 'Model:', self.model, 'id:', model_field.id if model_field else '-')
	#
	# 	do = self.normalize_do(do)
	#
	#
	#
	# 	# 1. Вытащить все роли юзверя.
	# 	roles = get_user_roles(self.user)
	# 	print('ROLES:', roles)
	#
	# 	for role in roles:
	# 		if role.name == 'superadmin':
	# 			return True
	#
	# 	# 2. Вытащить организацию юзера
	# 	organization_id = self.user.organization_id
	#
	# 	# 2. Проверить по массиву доступов - разрешено ли ему делать do с моделью model
	#
	# 	if model_field:
	# 		if hasattr(model_field, '_check_permissions'):
	# 			return model_field._check_permissions(self.user, roles, do)
	# 		else:
	# 			return self._check_field_level(roles, do, model_field)
	# 	else:
	# 		return self._check_table_level(roles, do)
	#
	#
	# def _check_table_level(self, roles, do):
	# 	"""Check permission by _role_permissions dict without organization or specific ID usage"""
	# 	model_name = self.model.__table__.name
	#
	# 	if model_name in _role_permissions_unix:
	# 		model_rights = _role_permissions_unix[model_name]
	# 	else:
	# 		model_rights = _role_permissions_unix['*']
	#
	# 	rp = model_rights['*']
	# 	for role in roles:
	# 		if role.name in model_rights:
	# 			rp2 = model_rights[role.name]
	# 			rp = _P(rp.user | rp2.user, rp.group | rp2.group, rp.others | rp2.others)
	# 	print(rp)
	# 	# Теперь у меня есть все пермишны для юзера с учётом ролей.
	# 	# Т.е. CRUD для юзера, группы и других. Нужно понять, куда относится юзер.
	# 	# По идее - нужно проверить родительскую таблицу. А для этого нужно дерево родительских таблиц
	# 	# И список пермишнов по каждой.
	# 	return True
	#
	#
	# 	rights = ''
	#
	# 	# Model level checker
	# 	if '*' in rp:
	# 		rights = rp['*']
	#
	# 	model = self.model.__table__.name
	#
	# 	if model in rp:
	# 		rights = rp[model]
	#
	# 	print('Model:', model, 'rights:', rights)
	#
	# 	if not rights:
	# 		return False
	#
	# 	doFirst = do[0:1].upper() # do can be: Create, Read, Update, Delete or C,R,U,D - use first symbol in upper case. Also it can be: Send (S),
	# 	print('doFirst:', doFirst)
	# 	return doFirst in rights
	#
	#
	# def _check_field_level(self, role, do, model_field):
	# 	"""Check access to concrete field."""
	#
	# 	# Пока следующее: если юзер - владелец - может всё.
	# 	# Как понять, что юзер = владелец.
	# 	# Как понять, что юзер - в той же группе, что и владелец записи?
	# 	if hasattr(model_field, 'get_permissions'):
	# 		perms = model_field.get_permissions(self.user, role, do)
	# 	else:
	# 		model = self.model.__table__.name
	# 		if model in _role_permissions_unix:
	# 			perms = _role_permissions_unix[model]['*']
	# 		else:
	# 			perms = _P(NONE, NONE, NONE)
	#
	# 	if self._check_user(self.user, model_field):
	# 		result_perms = perms.user
	# 	elif self._check_group(self.user, model_field):
	# 		result_perms = perms.group
	# 	else:
	# 		result_perms = perms.others
	#
	# 	if isinstance(do, str):
	# 		do = self.DO_RIGHTS[do[0:1].upper()]
	#
	# 	return bool(do | result_perms)
	#
	#
	#
	# def _check_user(self, user, model_field):
	# 	if hasattr(model_field, 'user_id'):
	# 		user_id = model_field.user_id
	# 	elif hasattr(model_field, 'user'):
	# 		user_id = model_field.user.id
	# 	elif hasattr(model_field, 'get_owner'):
	# 		user_id = model_field.get_owner()
	# 	else:
	# 		user_id = None
	#
	# 	if user_id and user.id == user_id:
	# 		return True
	# 	return False
	#
	#
	# def _check_group(self, user, model_field):
	# 	user_org_id = org_id = None
	# 	if hasattr(user, 'organization_id'):
	# 		user_org_id = user.organization_id
	# 	elif hasattr(user, 'organization'):
	# 		user_org_id = user.organization.id
	#
	# 	if user_org_id:
	# 		if hasattr(model_field, 'organization_id'):
	# 			org_id = model_field.organization_id
	# 		elif hasattr(model_field, 'get_owner_organization'):
	# 			org_id = model_field.get_owner_organization()
	# 		elif hasattr(model_field, 'organization'):
	# 			org_id = model_field.organization.id
	#
	# 		if org_id and user_org_id == org_id:
	# 			return True
	#
	# 	user_group_id = group_id = None
	# 	if hasattr(user, 'group_id'):
	# 		user_group_id = user.group_id
	# 	elif hasattr(user, 'group'):
	# 		user_group_id = user.group.id
	#
	# 	if user_group_id:
	# 		if hasattr(model_field, 'group_id'):
	# 			group_id = model_field.group_id
	# 		elif hasattr(model_field, 'get_owner_group'):
	# 			group_id = model_field.get_owner_group()
	# 		elif hasattr(model_field, 'group'):
	# 			group_id = model_field
	#
	# 		if group_id and user_group_id == group_id:
	# 			return True
	#
	# 	return False
	#
	#
	# def normalize_do(self, do):
	# 	if isinstance(do, int):
	# 		return do
	# 	result = NONE
	# 	if isinstance(do, str):
	# 		result = self.DO_RIGHTS[do[0:1].upper()]
	# 	elif isinstance(do, collections.Iterable):
	# 		for d in do:
	# 			if isinstance(d, str):
	# 				result |= self.DO_RIGHTS[d[0:1].upper()]
	# 			else:
	# 				result |= d
	# 	return result


"""


Переделать на следующее:
Unix style permissions:

Mine - My Organization's - Other's
00 = 0x0000 0000 = Do not has any access
01 = 0x0000 0001 = Can read (Read)
02 = 0x0000 0010 = Can add (Insert)
04 = 0x0000 0100 = Can change (Update)
08 = 0x0001 0000 = Can delete (Delete)
16 = 0x0010 0000 = Can send push-request

Если кол-во доступов более одного - флаги суммируются.
В БД хранить как unsigned int.

Но теперь у каждой записи должны быть: владелец и организация.
Если нет у текущей записи - проверяю родителей в дереве.

Проблема: у меня не дерево. User-point-ы могут относиться и к building-у и к user-у.
И таких приколов будет ещё.


building:
	floor:
		layer:
	control-point:
user:
	message
	user-point

"""



# _role_permissions = {
# 	'user': {
# 		'country': 'R',
# 		'city': 'R',
# 		'control_point': 'R',
# 		'control_point_type': 'R',
# 		'building': 'R',
# 		'floor': 'R',
# 		'layer': 'R',
# 		'layer_type': 'R',
# 		'shop_page': 'R',
# 		'warning': 'CR',
# 		'warning_type': 'R',
# 		'message': 'CR',
# 	},
# 	'content-manager': {
# 		'country': 'R',
# 		'city': 'R',
# 		'control_point': 'CRUD',
# 		'control_point_type': 'R',
# 		'building': 'CRUD',
# 		'floor': 'CRUD',
# 		'layer': 'CRUD',
# 		'layer_type': 'R',
# 		'shop_page': 'CRUD',
# 		'warning': 'R',
# 		'warning_type': 'R',
# 		'message': 'CR',
# 	},
# 	'admin': {
# 		'*': 'CRUDS',
# 	},
# 	'dispatcher': {
# 		# Аналогичен по сути юзеру - может смотреть всё, но в добавок ещё и полную карту.
# 		'country': 'R',
# 		'city': 'R',
# 		'control_point': 'R',
# 		'control_point_type': 'R',
# 		'building': 'R',
# 		'floor': 'R',
# 		'layer': 'R',
# 		'layer_type': 'R',
# 		'shop_page': 'R',
# 		'warning': 'CRUDS',
# 		'warning_type': 'R',
# 		'message': 'CR',
# 	}
# }
#
# _P = collections.namedtuple('Permission', ('user', 'group', 'others'))
#
# # Permission constants
# NONE = 0x00
# READ = 0x01
# CREATE = 0x02
# UPDATE = 0x04
# DELETE = 0x08
# SEND = 0x10
#
# CR = CREATE | READ                      # = 0x3
# CRU = CREATE | READ | UPDATE            # = 0x7
# RUD = READ | UPDATE | DELETE            # = 0xD
# CRUD = CREATE | READ | UPDATE | DELETE  # = 0x0f = 15 = 0b00001111
# ALL = 0xFF
#
#
#
# P_CRR = _P(CRUD, READ, READ)
# P_RRR = _P(RUD, READ, READ)
# P_CCR = _P(CRUD, CRUD, READ)
#
# _role_permissions_unix = {
# 	# Mine, My Organization's or My Group's, Other's
# 	# Named tuple, which means: user, group, others, receiver
# 	'*': {'*': _P(NONE, NONE, NONE)},
# 	'country': {'*': P_RRR},
# 	'city': {'*': P_RRR},
# 	'control_point_type': {'*': P_RRR},
# 	'layer_type': {'*': P_RRR},
# 	'warning_type': {'*': P_RRR},
#
# 	'control_point': {'*': P_RRR, 'editor': P_CCR, 'admin': P_CCR},
# 	'building': {'*': P_RRR, 'editor': P_CCR, 'admin': P_CCR},
# 	'floor': {'*': P_RRR, 'editor': P_CCR, 'admin': P_CCR},
# 	'layer': {'*': P_RRR, 'editor': P_CCR, 'admin': P_CCR},
# 	'shop_page': {'*': P_RRR},
# 	'warning': {'*': _P(CR, READ, READ), 'dispatcher': _P(CRUD | SEND, CRUD | SEND, READ)},
# 	'message': {'*': _P(CR, READ, NONE)},
# 	'message_attachment': {'*': _P(CRU, READ, NONE)},
# 	'message_receiver_unread': {'*': _P(READ, READ, NONE)},
# 	'user': {'*': _P(CRUD, CR, NONE), 'admin': P_CCR},
# 	'user_point': {'*': P_RRR}
# }
#
#
# class Permission:
#
# 	DO_RIGHTS = {
# 		'R': READ,
# 		'C': CREATE,
# 		'U': UPDATE,
# 		'D': DELETE,
# 		'S': SEND,
# 	}
#
#
# 	def __init__(self, user, model=None):
# 		self.user = user
# 		self.model = model
#
#
# 	def can(self, do=READ, model_field=None):
# 		if not self.user:
# 			return False
#
# 		print('User:', self.user, 'DO:', do, 'Model:', self.model, 'id:', model_field.id if model_field else '-')
#
# 		do = self.normalize_do(do)
#
# 		# 1. Вытащить все роли юзверя.
# 		roles = get_user_roles(self.user)
# 		print('ROLES:', roles)
#
# 		for role in roles:
# 			if role.name == 'superadmin':
# 				return True
#
# 		# 2. Вытащить организацию юзера
# 		organization_id = self.user.organization_id
#
# 		# 2. Проверить по массиву доступов - разрешено ли ему делать do с моделью model
#
# 		if model_field:
# 			if hasattr(model_field, '_check_permissions'):
# 				return model_field._check_permissions(self.user, roles, do)
# 			else:
# 				return self._check_field_level(roles, do, model_field)
# 		else:
# 			return self._check_table_level(roles, do)
#
#
# 	def _check_table_level(self, roles, do):
# 		"""Check permission by _role_permissions dict without organization or specific ID usage"""
# 		model_name = self.model.__table__.name
#
# 		if model_name in _role_permissions_unix:
# 			model_rights = _role_permissions_unix[model_name]
# 		else:
# 			model_rights = _role_permissions_unix['*']
#
# 		rp = model_rights['*']
# 		for role in roles:
# 			if role.name in model_rights:
# 				rp2 = model_rights[role.name]
# 				rp = _P(rp.user | rp2.user, rp.group | rp2.group, rp.others | rp2.others)
# 		print(rp)
# 		# Теперь у меня есть все пермишны для юзера с учётом ролей.
# 		# Т.е. CRUD для юзера, группы и других. Нужно понять, куда относится юзер.
# 		# По идее - нужно проверить родительскую таблицу. А для этого нужно дерево родительских таблиц
# 		# И список пермишнов по каждой.
# 		return True
#
#
# 		rights = ''
#
# 		# Model level checker
# 		if '*' in rp:
# 			rights = rp['*']
#
# 		model = self.model.__table__.name
#
# 		if model in rp:
# 			rights = rp[model]
#
# 		print('Model:', model, 'rights:', rights)
#
# 		if not rights:
# 			return False
#
# 		doFirst = do[0:1].upper() # do can be: Create, Read, Update, Delete or C,R,U,D - use first symbol in upper case. Also it can be: Send (S),
# 		print('doFirst:', doFirst)
# 		return doFirst in rights
#
#
# 	def _check_field_level(self, role, do, model_field):
# 		"""Check access to concrete field."""
#
# 		# Пока следующее: если юзер - владелец - может всё.
# 		# Как понять, что юзер = владелец.
# 		# Как понять, что юзер - в той же группе, что и владелец записи?
# 		if hasattr(model_field, 'get_permissions'):
# 			perms = model_field.get_permissions(self.user, role, do)
# 		else:
# 			model = self.model.__table__.name
# 			if model in _role_permissions_unix:
# 				perms = _role_permissions_unix[model]['*']
# 			else:
# 				perms = _P(NONE, NONE, NONE)
#
# 		if self._check_user(self.user, model_field):
# 			result_perms = perms.user
# 		elif self._check_group(self.user, model_field):
# 			result_perms = perms.group
# 		else:
# 			result_perms = perms.others
#
# 		if isinstance(do, str):
# 			do = self.DO_RIGHTS[do[0:1].upper()]
#
# 		return bool(do | result_perms)
#
#
#
# 	def _check_user(self, user, model_field):
# 		if hasattr(model_field, 'user_id'):
# 			user_id = model_field.user_id
# 		elif hasattr(model_field, 'user'):
# 			user_id = model_field.user.id
# 		elif hasattr(model_field, 'get_owner'):
# 			user_id = model_field.get_owner()
# 		else:
# 			user_id = None
#
# 		if user_id and user.id == user_id:
# 			return True
# 		return False
#
#
# 	def _check_group(self, user, model_field):
# 		user_org_id = org_id = None
# 		if hasattr(user, 'organization_id'):
# 			user_org_id = user.organization_id
# 		elif hasattr(user, 'organization'):
# 			user_org_id = user.organization.id
#
# 		if user_org_id:
# 			if hasattr(model_field, 'organization_id'):
# 				org_id = model_field.organization_id
# 			elif hasattr(model_field, 'get_owner_organization'):
# 				org_id = model_field.get_owner_organization()
# 			elif hasattr(model_field, 'organization'):
# 				org_id = model_field.organization.id
#
# 			if org_id and user_org_id == org_id:
# 				return True
#
# 		user_group_id = group_id = None
# 		if hasattr(user, 'group_id'):
# 			user_group_id = user.group_id
# 		elif hasattr(user, 'group'):
# 			user_group_id = user.group.id
#
# 		if user_group_id:
# 			if hasattr(model_field, 'group_id'):
# 				group_id = model_field.group_id
# 			elif hasattr(model_field, 'get_owner_group'):
# 				group_id = model_field.get_owner_group()
# 			elif hasattr(model_field, 'group'):
# 				group_id = model_field
#
# 			if group_id and user_group_id == group_id:
# 				return True
#
# 		return False
#
#
# 	def normalize_do(self, do):
# 		if isinstance(do, int):
# 			return do
# 		result = NONE
# 		if isinstance(do, str):
# 			result = self.DO_RIGHTS[do[0:1].upper()]
# 		elif isinstance(do, collections.Iterable):
# 			for d in do:
# 				if isinstance(d, str):
# 					result |= self.DO_RIGHTS[d[0:1].upper()]
# 				else:
# 					result |= d
# 		return result




#def permission_check(func):
	#"""Decorator that checking a permission by my Permission model."""
	#@wraps(func)
	#def wrapper(*args, **kwargs):
		#if not getattr(func, 'authenticated', True):
			#return func(*args, **kwargs)

		#acct = basic_authentication()  # custom account lookup function

		#if acct:
			#return func(*args, **kwargs)

		#restful.abort(401)
	#return wrapper


"""

SELECT role.id AS role_id, role.name AS role_name 
FROM role
JOIN group_role ON role.id = group_role.role_id
JOIN "group" ON "group".id = group_role.group_id
JOIN user_group ON "group".id = user_group.group_id
JOIN user ON user.id = user_group.user_id 
WHERE user.id = ?

"""

"""
Что мне нужно прописать.
У каждого модуля свои настройки разрешений.
- Каждый модуль кидает мне в базу - какие разрешения он предоставляет.
А я в системе ролей уже настраиваю, какой роли какие пермишны раздать.
1. У каждой записи должен быть владелец и организация-владелец.

- Модуль Editor:
	- 
- Модуль Monitor;
	-
- Модуль API:
	- 
"""


