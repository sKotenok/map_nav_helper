from flask_jwt_extended import jwt_required, jwt_optional
from flask import json, current_app, url_for
from marshmallow import pre_load, post_load

from extensions import ma, db
from common.model_resource import (ModelResourceDetail, ModelResourceList,
	ModelResourceListByParent, ModelResourceMany, ModelFile)
from common.filters import filter_latest_by_user, filter_last_time, order_by_field, filter_from
from models.building import (Building, City, Country, Floor, FloorBitmap)
from models.geodata import GeoObject, GeoObjectType, GeoProperty, GeoPropertyValue

from .permissions import BuildingPermission, FloorPermission, GeoObjectPermission
from common.permission import DirectoryPermission
from common.file_utils import get_image_props, plan_tile_generation


class CountrySchema(ma.ModelSchema):
	class Meta:
		fields = ('id', 'name')
		model = Country
		sqla_session = db.session

class CountryResource(ModelResourceDetail):
	model = Country
	schema = CountrySchema
	permission = DirectoryPermission

class CountryList(ModelResourceList):
	model = Country
	schema = CountrySchema
	permission = DirectoryPermission



class CitySchema(ma.ModelSchema):
	class Meta:
		fields = ('id', 'name', 'country_id')
		model = City
		sqla_session = db.session

class CityResource(ModelResourceDetail):
	model = City
	schema = CitySchema
	permission = DirectoryPermission

class CityList(ModelResourceList):
	model = City
	schema = CitySchema
	permission = DirectoryPermission

class CountryCityList(ModelResourceListByParent):
	base_model = Country
	child_model = City
	child_schema = CitySchema
	relations = { 'parent_id': 'country_id', 'childs': 'cities' }
	permission = DirectoryPermission




class BuildingSchema(ma.ModelSchema):
	geodata = ma.Function(lambda obj: json.loads(obj.geodata) if obj.geodata else None)
	time_updated = ma.DateTime(dump_only=True)

	class Meta:
		fields = ('id', 'city_id', 'address', 'name', 'short_description', 'geodata', 'coords', 'time_updated')
		model = Building
		sqla_session = db.session


class BuildingResource(ModelResourceDetail):
	model = Building
	schema = BuildingSchema
	permission = BuildingPermission

class BuildingList(ModelResourceList):
	model = Building
	schema = BuildingSchema
	permission = BuildingPermission

class CityBuildingList(ModelResourceListByParent):
	base_model = City
	child_model = Building
	child_schema = BuildingSchema
	relations = { 'parent_id': 'city_id', 'childs': 'buildings' }
	permission = BuildingPermission


class FloorSchema(ma.ModelSchema):
	geodata = ma.Function(lambda obj: json.loads(obj.geodata) if obj.geodata else None)
	time_updated = ma.DateTime(dump_only=True)

	class Meta:
		fields = ('id', 'building_id', 'position', 'name', 'geodata', 'bitmaps', 'time_updated')
		model = Floor
		sqla_session = db.session

	# @pre_load
	# def preprocess(self, data):
	# 	print('Data after cycle:', data)
	# 	return data


class FloorResource(ModelResourceDetail):
	model = Floor
	schema = FloorSchema
	permission = FloorPermission

class FloorList(ModelResourceList):
	model = Floor
	schema = FloorSchema
	permission = FloorPermission

class BuildingFloorList(ModelResourceListByParent):
	base_model = Building
	child_model = Floor
	child_schema = FloorSchema
	relations = { 'parent_id': 'building_id', 'childs': 'floors' }
	permission = FloorPermission



class GeoObjectSchema(ma.ModelSchema):

	class Meta:
		fields = ('id', 'type_id', 'building_id', 'floor_id', 'geodata', 'organization_id', 'time_updated', 'properties')
		model = Floor
		sqla_session = db.session


class GeoObjectResource(ModelResourceDetail):
	model = GeoObject
	schema = GeoObjectSchema
	permission = GeoObjectPermission

class GeoObjectList(ModelResourceList):
	model = GeoObject
	schema = GeoObjectSchema
	permission = GeoObjectPermission

class FloorGeoObjectsList(ModelResourceListByParent):
	base_model = Floor
	child_model = GeoObject
	child_schema = GeoObjectSchema
	relations = { 'parent_id': 'floor_id', 'childs': 'geo_objects' }
	permission = GeoObjectPermission



class FloorBitmapSchema(ma.ModelSchema):
	class Meta:
		model=FloorBitmap
		sqla_session = db.session


class BitmapList(ModelResourceList):
	model = FloorBitmap
	schema = FloorBitmapSchema

class FloorFloorBitmaps(ModelResourceListByParent):
	base_model = Floor
	child_model = FloorBitmap
	child_schema = FloorBitmapSchema
	relations = { 'parent_id': 'floor_id', 'childs': 'bitmaps' }
	permission = GeoObjectPermission


class FloorImageUpload(ModelFile):
	path_to_save = 'BITMAPS'
	model = FloorBitmap
	schema = FloorBitmapSchema
	permission = GeoObjectPermission
	after_upload = [get_image_props, plan_tile_generation]






