
from flask import Blueprint, json, make_response
from flask_restful import Api


from .resources import (
	BuildingResource, BuildingList, CityBuildingList,
	FloorResource, FloorList, BuildingFloorList,
	CityResource, CityList, CountryCityList,
	CountryResource, CountryList,
	GeoObjectResource, GeoObjectList, FloorGeoObjectsList,
	BitmapList, FloorImageUpload
	#	UserPointResource, UserPointList, FloorUserPointsList, UserUserPointsList, UserPointMany,
	#	WarningResource, WarningList, FloorWarningsList, BuildingWarningsList, WarningTypeList
)


def api_urls(api, blueprint):

	api.add_resource(CountryResource, '/country/<int:model_id>')
	api.add_resource(CountryList, '/countries')

	api.add_resource(CityResource, '/city/<int:model_id>')
	api.add_resource(CityList, '/cities')
	api.add_resource(CountryCityList, '/country/<int:model_id>/cities')

	api.add_resource(BuildingResource, '/building/<int:model_id>')
	api.add_resource(BuildingList, '/buildings')
	api.add_resource(CityBuildingList, '/city/<int:model_id>/buildings')

	api.add_resource(FloorResource, '/floor/<int:model_id>')
	api.add_resource(FloorList, '/floors')
	api.add_resource(BuildingFloorList, '/building/<int:model_id>/floors')

	api.add_resource(GeoObjectResource, '/geo-object/<int:model_id>')
	api.add_resource(GeoObjectList, '/geo-objects')
	api.add_resource(FloorGeoObjectsList, '/floor/<int:model_id>/geo-objects')

	api.add_resource(BitmapList, '/bitmaps')
	api.add_resource(FloorImageUpload, '/bitmap/upload-image')

# api.add_resource(ObjectResource, '/control-point/<int:model_id>')
# api.add_resource(ControlPointList, '/control-points')
# api.add_resource(BuildingControlPointsList, '/building/<int:model_id>/control-points')
# api.add_resource(FloorControlPointsList, '/floor/<int:model_id>/control-points')
#
# api.add_resource(ControlPointTypeResource, '/control-point-type/<int:model_id>')
# api.add_resource(ControlPointTypeList, '/control-point-types')
#
#
# api.add_resource(ShopPageResource, '/shop-page/<int:model_id>')
# api.add_resource(ShopPageList, '/shop-pages')
# api.add_resource(FloorShopPagesList, '/floor/<int:model_id>/shop-pages')
#
#
# api.add_resource(UserPointResource, '/user-point/<int:model_id>')
# api.add_resource(UserPointList, '/user-points')
# api.add_resource(UserPointMany, '/user-points/load')
# api.add_resource(FloorUserPointsList, '/floor/<int:model_id>/user-points')
# api.add_resource(UserUserPointsList, '/user/<int:model_id>/user-points')


# api.add_resource(WarningTypeList, '/warning-types')
# api.add_resource(WarningResource, '/warning/<int:model_id>')
# api.add_resource(WarningList, '/warnings')
# api.add_resource(FloorWarningsList, '/floor/<int:model_id>/warnings')
# api.add_resource(BuildingWarningsList, '/building/<int:model_id>/warnings')





