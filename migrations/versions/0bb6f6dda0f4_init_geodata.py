"""Init Geodata

Revision ID: 0bb6f6dda0f4
Revises: 085c04500638
Create Date: 2018-09-13 16:44:48.594870

manage.py db revision --autogenerate -m "Init Geodata"
"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '0bb6f6dda0f4'
down_revision = '085c04500638'
branch_labels = None
depends_on = None


def upgrade():
	op.create_table('country',
		sa.Column('id', sa.Integer(), nullable=False),
		sa.Column('name', sa.String(length=100), nullable=False, server_default=''),
		sa.PrimaryKeyConstraint('id')
	)

	op.create_table('city',
		sa.Column('id', sa.Integer(), nullable=False),
		sa.Column('name', sa.String(length=100), nullable=False),
		sa.Column('country_id', sa.Integer(), nullable=False),
		sa.ForeignKeyConstraint(('country_id',), ['country.id'], ),
		sa.PrimaryKeyConstraint('id')
	)

	op.create_table('building',
		sa.Column('id', sa.Integer(), nullable=False),
		sa.Column('active', sa.Boolean(), nullable=False, server_default=sa.literal(True)),
		sa.Column('city_id', sa.Integer(), nullable=False),
		sa.Column('organization_id', sa.Integer(), nullable=False),
		sa.Column('address', sa.String(length=300), nullable=False, server_default=''),
		sa.Column('name', sa.String(length=80), nullable=False, server_default=''),
		sa.Column('short_description', sa.Text(), nullable=False, server_default=''),
		sa.Column('geodata', sa.Text(), nullable=True),
		sa.Column('coords_lat', sa.Numeric(precision=16, scale=9, asdecimal=False), nullable=False),
		sa.Column('coords_lng', sa.Numeric(precision=16, scale=9, asdecimal=False), nullable=False),
		sa.Column('time_created', sa.TIMESTAMP(timezone=True), server_default=sa.text('now()'), nullable=False),
		sa.Column('time_updated', sa.TIMESTAMP(timezone=True), server_default=sa.text('now()'), nullable=False),
		sa.ForeignKeyConstraint(('city_id',), ['city.id'], ),
		sa.ForeignKeyConstraint(('organization_id',), ['organization.id'], ),
		sa.PrimaryKeyConstraint('id')
	)

	op.create_table('floor',
		sa.Column('id', sa.Integer(), nullable=False),
		sa.Column('active', sa.Boolean(), nullable=False, server_default=sa.literal(True)),
		sa.Column('building_id', sa.Integer(), nullable=False),
		sa.Column('name', sa.String(length=100), nullable=False, server_default=''),
		sa.Column('position', sa.Integer(), nullable=False, server_default='0'),
		sa.Column('geodata', sa.Text(), nullable=True),
		sa.Column('time_created', sa.TIMESTAMP(timezone=True), server_default=sa.text('now()'), nullable=False),
		sa.Column('time_updated', sa.TIMESTAMP(timezone=True), server_default=sa.text('now()'), nullable=False),
		sa.ForeignKeyConstraint(('building_id',), ['building.id'], ),
		sa.PrimaryKeyConstraint('id')
	)

	op.create_table('floor_bitmaps',
		sa.Column('id', sa.Integer(), nullable=False),
		sa.Column('type', sa.String(length=10), nullable=False),
		sa.Column('url', sa.String(length=1000), nullable=True),
		sa.Column('width', sa.Integer(), nullable=False),
		sa.Column('height', sa.Integer(), nullable=False),
		sa.Column('scale', sa.Float(), nullable=False),
		sa.Column('floor_id', sa.Integer(), nullable=False),
		sa.ForeignKeyConstraint(('floor_id',), ['floor.id'], ),
		sa.PrimaryKeyConstraint('id')
	)

	op.create_table('geo_object_type',
		sa.Column('id', sa.Integer(), nullable=False),
		sa.Column('name', sa.String(length=100), nullable=False, server_default=''),
		sa.Column('geo_type', sa.String(length=100), nullable=False, server_default='point'),
		sa.PrimaryKeyConstraint('id')
	)

	op.create_table('geo_property',
		sa.Column('id', sa.Integer(), nullable=False),
		sa.Column('name', sa.String(length=100), nullable=False, server_default=''),
		sa.Column('type', sa.String(length=100), nullable=False),
		sa.PrimaryKeyConstraint('id')
	)

	op.create_table('geo_object',
		sa.Column('id', sa.Integer(), nullable=False),
		sa.Column('geodata', sa.Text(), nullable=True),
		sa.Column('type_id', sa.Integer(), nullable=False),
		sa.Column('floor_id', sa.Integer(), nullable=False),
		sa.Column('building_id', sa.Integer(), nullable=True),
		sa.Column('organization_id', sa.Integer(), nullable=True),
		sa.Column('time_created', sa.TIMESTAMP(timezone=True), server_default=sa.text('now()'), nullable=False),
		sa.Column('time_updated', sa.TIMESTAMP(timezone=True), server_default=sa.text('now()'), nullable=False),
		sa.ForeignKeyConstraint(('building_id',), ['building.id'], ),
		sa.ForeignKeyConstraint(('floor_id',), ['floor.id'], ),
		sa.ForeignKeyConstraint(('organization_id',), ['organization.id'], ),
		sa.ForeignKeyConstraint(('type_id',), ['geo_object_type.id'], ),
		sa.PrimaryKeyConstraint('id')
	)

	op.create_table('geo_property_value',
		sa.Column('id', sa.Integer(), nullable=False),
		sa.Column('value', sa.Text(), nullable=True),
		sa.Column('geo_object_id', sa.Integer(), nullable=False),
		sa.Column('geo_property_id', sa.Integer(), nullable=False),
		sa.ForeignKeyConstraint(('geo_object_id',), ['geo_object.id'], ),
		sa.ForeignKeyConstraint(('geo_property_id',), ['geo_property.id'], ),
		sa.PrimaryKeyConstraint('id')
	)


def downgrade():
	op.drop_table('geo_property_value')
	op.drop_table('geo_object')
	op.drop_table('floor_bitmaps')
	op.drop_table('shop')
	op.drop_table('floor')
	op.drop_table('building')
	op.drop_table('city')
	op.drop_table('geo_property')
	op.drop_table('geo_object_type')
	op.drop_table('country')
