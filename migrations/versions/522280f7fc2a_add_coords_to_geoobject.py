"""Add Coords to GeoObject

Revision ID: 522280f7fc2a
Revises: 4f5a560dc49f
Create Date: 2018-11-13 08:16:39.869186

./manage.py db revision --autogenerate -m "Add Coords to GeoObject"
"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '522280f7fc2a'
down_revision = '4f5a560dc49f'
branch_labels = None
depends_on = None


def upgrade():
	op.add_column('geo_object', sa.Column('coords_lat', sa.Numeric(precision=16, scale=9, asdecimal=False), nullable=False))
	op.add_column('geo_object', sa.Column('coords_lng', sa.Numeric(precision=16, scale=9, asdecimal=False), nullable=False))


def downgrade():
	op.drop_column('geo_object', 'coords_lng')
	op.drop_column('geo_object', 'coords_lat')
