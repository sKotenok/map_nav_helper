from extensions import ma, db
from sqlalchemy import and_, desc
import timestring






def order_by_field(self, query, model, request, *args, **kwargs):
	order_by = request.args.get('order-by', None)
	order_by_desc = request.args.get('order-by-desc', None)
	if not (order_by or order_by_desc): return query

	if order_by and (hasattr(model, order_by)):
		query = query.order_by(getattr(model, order_by))
	elif order_by_desc and (hasattr(model, order_by_desc)):
		query = query.order_by(desc(getattr(model, order_by_desc)))
	return query


def filter_from(self, query, model, request, *args, **kwargs):
	time_from = request.args.get('from', None)
	if not time_from:
		return query

	t = timestring.findall(time_from)
	if not t or not (len(t) > 0):
		return query

	time_parsed = str( t[0][1] )
	query = query.filter(model.time_created >= time_parsed)
	return query


def filter_last_time(self, query, model, request, *args, **kwargs):
	time_filter = request.args.get('time-filter', None)
	if not time_filter:
		return query

	time_parsed = timestring.Date('now') - time_filter

	if not time_parsed:
		print('TIME NOT PARSED')
		return query

	print('FILTER LAST TIME', time_parsed)

	if isinstance(time_parsed, timestring.Date):
		time_parsed = str(time_parsed)
		if time_parsed and time_parsed != 'infinity':
			query = query.filter(model.time_created >= str(time_parsed))
	# elif isinstance(time_parsed, timestring.Range):
	# 	start, end = (str(t) for t in time_parsed)
	# 	if start and start != 'infinity':
	# 		query = query.filter(model.time_created >= start)
	# 	if end and end != 'infinity':
	# 		query = query.filter(model.time_created <= end)
	return query


def filter_latest_by_user(self, query, model, request, parent_id=None, *args, **kwargs):
	"""
	Если прислали from - фильтрует только записи старше присланной даты
	Если прислали аргумент latest, dыполняет запрос:
SELECT * FROM user_point
WHERE user_point.floor_id = 2
AND user_point.time_created IN (SELECT max(user_point.time_created) AS max_1 FROM user_point GROUP BY user_point.user_id)
LIMIT 0, 500

Предыдущий вариант запроса: слишком медленный!
SELECT * FROM `user_point`
LEFT JOIN `user_point` as `up2`
ON (`user_point`.user_id=`up2`.user_id AND (`user_point`.time_created < `up2`.time_created))
WHERE `up2`.user_id IS NULL


SELECT *
FROM user_point
WHERE 2 = user_point.floor_id
AND user_point.time_created IN (
		SELECT max(user_point.time_created) AS max_1
		FROM user_point
		WHERE user_point.floor_id = 2
		GROUP BY user_point.user_id
	)
AND user_point.time_created >= '2018-06-26 12:05:47.461198'
LIMIT 0, 500


SELECT user_point.id AS user_point_id, user_point.user_id AS user_point_user_id, user_point.floor_id AS user_point_floor_id, user_point.coords_lat AS user_point_coords_lat, user_point.coords_lng AS user_point_coords_lng, user_point.time_created AS user_point_time_created
FROM user_point
WHERE
2 = user_point.floor_id
AND user_point.time_created >= '22018-07-24 13:57:40.257716'
AND user_point.time_created IN (
	SELECT max(user_point.time_created) AS max_1
	FROM user_point
	WHERE user_point.floor_id = 2 GROUP BY user_point.user_id
)
LIMIT 0, 500
{'param_1': 2, 'time_created_1': '2018-07-24 17:14:15.257716', 'floor_id_1': 2, 'param_2': 0, 'param_3': 500}

SELECT user_point.id AS user_point_id, user_point.user_id AS user_point_user_id, user_point.floor_id AS user_point_floor_id, user_point.coords_lat AS user_point_coords_lat, user_point.coords_lng AS user_point_coords_lng, user_point.time_created AS user_point_time_created
FROM user_point
WHERE %(param_1)s = user_point.floor_id AND user_point.time_created >= %(time_created_1)s AND user_point.time_created IN (SELECT max(user_point.time_created) AS max_1
FROM user_point
WHERE user_point.floor_id = %(floor_id_1)s GROUP BY user_point.user_id)
 LIMIT %(param_2)s, %(param_3)s
2018-07-25 10:39:17,827 INFO sqlalchemy.engine.base.Engine {'param_1': 2, 'time_created_1': '2018-07-25 10:38:32.823940', 'floor_id_1': 2, 'param_2': 0, 'param_3': 500}



	"""
	latest_only = request.args.get('latest', False)
	if int(latest_only):
		subq = db.session.query(db.func.max(model.time_created)).group_by(model.user_id)
		if parent_id and hasattr(self, 'relations'):
			subq = subq.filter(getattr(model, self.relations['parent_id']) == parent_id)
		query = query.filter(model.time_created.in_(subq))
	# Too slow:
	#model_1 = aliased(model)
	#query = (query
	#.outerjoin(model_1, and_(model_1.user_id == model.user_id, model.time_created < model_1.time_created))
	#.filter(model_1.user_id==None))
	return query
