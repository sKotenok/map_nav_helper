# Define permissions, allowed for this module.

"""

Роли:

root - может всё.

building admin - может создавать, редактировать учётки юзеров своей организации, настраивать прова по внутренним группам,
	- добавлять магазины и связанные с ними учётки (в форме магазина: добавить владельца).
	Не может: редактировать роли и пользователей внутри магазинов, привязанных к своему зданию, кроме функции "заблокировать
		доступ к магазину в моём здании".

shop admin - создавать, редактировать учётки юзеров своего магазина, настраивать права по внутр. группам.

простой смертный - может редактировать только свою учётку, регистрироваться.



Процесс проверки прав доступа:
1. Найти организации юзера. Если нет - в набор условий простого смертного.
	- Юзер из организации:
	2. Найти здания, которыми владеет организация.
	3. Найти магазины, которыми владеет организация.
	4. Найти роли юзера внутри организации (ограничения, наложенные админом этой орг-ции, самому админу можно всё)
	= 5. Вытащить информацию о получаемом обьекте - какой организации он принадлежит.
	6. Проверить, есть ли у данного юзера нужный доступ к обьекту.

	- простой смертный:
	2. Проверить, доступно ли данное действие простому авторизованному юзеру.

"""
from models.auth import User, Organization, Role
from models.building import Building


class Permission:

	def can(self, do, user, model):
		if user.organization_id:
			if model.organization_id == user.organization_id:
				if self.check_app(do, user, model):
					return self.check_rbac(do, user, model)
			return False
		pass

	def check_rbac(self, do, user, model):
		return True

	def check_app(self, do, user, model):
		return True

	def get_all_permissions(self, user_id):
		if user_id:
			user = User.find(user_id)
			if user.organization_id:
				# Пользователь относится к одной из зареганых компаний.
				organization = user.organization
				buildings = organization.buildings
				if buildings:
					# Компания "владеет" хотя бы одним зданием.
					pass
				shops = organization.shops
				if shops:
					# Комания "владеет" хотя бы одним магазином.
					pass

			else:
				# Простой авторизованный пользователь.
				pass
		else:
			# Простой неавторизованный пользователь.
			pass









