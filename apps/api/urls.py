from flask import Blueprint, json, make_response
from flask_restful import Api

from common.routes_table import RoutesTable


blueprint = Blueprint('api', __name__, url_prefix='/api/v2')

api = Api(blueprint)


@api.representation('application/json')
def output_json(data, code, headers=None):
	resp = make_response(json.jsonify(data), code)
	resp.headers.extend(headers or {})
	return resp

api.add_resource(RoutesTable, '/')


from apps.auth import urls_api as auth_api
auth_api.api_urls(api, blueprint)

from apps.geodata import urls_api as geodata_api
geodata_api.api_urls(api, blueprint)



