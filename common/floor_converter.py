


def convert_floor(img_file, center, scale, rotate, tile_levels):
	"""Конвертирует битмапу в набор тайлов
	img_file - ссылка на файл,
	center, scale, rotate - положение битмапа на карте: координаты [lat,lng], масштаб, угол поворота
	tile_levels - требуемые уровни масштаба сгенерированных тайлов.
	"""
	# 1. Приведём битмап к нужной форме: отмасштабировать, повернуть.
	# Для каждого zoom_level:
	#   2. Вычислить нумерацию тайловой сетки вокруг изображения
	#   3. Добавить прозрачного цвета, чтобы битмап точно встал по границам сетки
	#   4. Нарезать на тайлы по вычесленным границам.
	#   5. Сохранить по заданным путям.



def _rotate_image(img_file, save_as_file, rotate_angle, scale):
	"""Поворачивает картинку на необх. угол и масштабирует"""
	from PIL import Image

	src_img = Image.open(img_file)

	img = src_img.convert('RGBA')
	if scale:
		width, height = img.size
		img = img.resize((int(width * scale), int(height * scale)))
		print('New size:', width * scale, height * scale)

	rot_img = img.rotate(rotate_angle, expand=1)

	dst_img = Image.new("RGBA", rot_img.size, (255, 0, 0, 0))
	dst_img.paste(rot_img, (0, 0), rot_img)
	dst_img.save(save_as_file, 'PNG')


def meters_to_pixels(center, size, zoom):
	""""""
	pass


if __name__ == '__main__':
	_rotate_image('test1.png', 'result1.png', 45, 0.5)


