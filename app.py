from flask import Flask

from extensions import db, jwt, migrate, fcm


def create_app(config=None, testing=False, cli=False):
	"""Application factory, used to create application"""
	app = Flask('nav_helper_flask')

	configure_app(app, testing)
	configure_extensions(app, cli)
	register_blueprints(app)

	return app


def create_wsgi_app(config=None, testing=False, cli=False):
	""""""
	class ReverseProxied(object):
		'''Wrap the application in this middleware and configure the
		front-end server to add these headers, to let you quietly bind
		this to a URL other than / and to an HTTP scheme that is
		different than what is used locally.

		In nginx:
		location /myprefix {
			proxy_pass http://192.168.0.1:5001;
			proxy_set_header Host $host;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_set_header X-Scheme $scheme;
			proxy_set_header X-Script-Name /myprefix;
			}

		:param app: the WSGI application
		'''
		def __init__(self, app):
			self.app = app

		def __call__(self, environ, start_response):
			script_name = environ.get('HTTP_X_SCRIPT_NAME', '')
			if script_name:
				environ['SCRIPT_NAME'] = script_name
				path_info = environ['PATH_INFO']
				if path_info.startswith(script_name):
					environ['PATH_INFO'] = path_info[len(script_name):]

			scheme = environ.get('HTTP_X_SCHEME', '')
			if scheme:
				environ['wsgi.url_scheme'] = scheme
			return self.app(environ, start_response)

	app = create_app()
	app.wsgi_app = ReverseProxied(app.wsgi_app)
	return app


def configure_app(app, testing=False):
	"""set configuration for application"""
	# default configuration
	app.config.from_object('config')

	if testing is True:
		# override with testing config
		app.config.from_object('configtest')
	else:
		# override with env variable, fail silently if not set
		app.config.from_envvar("API_CONFIG", silent=True)


def configure_extensions(app, cli):
	"""configure flask extensions"""
	db.init_app(app)
	jwt.init_app(app)
	fcm.init_app(app)

	if cli is True:
		migrate.init_app(app, db)


def register_blueprints(app):
	"""register all blueprints for application"""
	from apps import api, auth, editor, main, message, monitor, catalog
	app.register_blueprint(api.blueprint)
	app.register_blueprint(editor.blueprint)
	app.register_blueprint(auth.blueprint)
	app.register_blueprint(catalog.blueprint)
# app.register_blueprint(monitor.blueprint)
	# app.register_blueprint(main.blueprint)
	# app.register_blueprint(message.blueprint)

