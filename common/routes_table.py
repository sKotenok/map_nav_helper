from flask_restful import Resource
from flask import current_app
from operator import attrgetter


class RoutesTable(Resource):
	"""Show all allowed site routes"""

	def _get_routes(self, sort='rule', all_methods=True):
		rules = list(current_app.url_map.iter_rules())
		if not rules:
			return {'msg': 'No routes were registered.'}

		ignored_methods = set(() if all_methods else ('HEAD', 'OPTIONS'))

		if sort in ('endpoint', 'rule'):
			rules = sorted(rules, key=attrgetter(sort))
		elif sort == 'methods':
			rules = sorted(rules, key=lambda rule: sorted(rule.methods))

		rule_methods = {}
		for r in rules:
			rule_methods[r.rule] = ','.join([m for m in sorted(r.methods - ignored_methods)])

		return rule_methods

	def get(self):
		return self._get_routes(sort='endpoint')
