from extensions import db
from common.base_model import OwnerMixin
import hashlib


class Country(db.Model, OwnerMixin):
	"""Country model"""
	__tablename__ = 'country'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100), nullable=False)

	def __repr__(self):
		return "<Country %d %s>" % (self.id, self.name)

	# У общеих справочников нет владельца, поэтому изменить их может только root
	def get_owner_organization(self): return None
	def get_owner_user(self): return None


class City(db.Model, OwnerMixin):
	"""City model"""
	__tablename__ = 'city'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100), nullable=False)

	country_id = db.Column(db.Integer, db.ForeignKey('country.id'), nullable=False)
	country = db.relationship('Country', backref=db.backref('cities', lazy='dynamic'))

	def __repr__(self):
		return "<City %d %s>" % (self.id, self.name)

	# У общеих справочников нет владельца, поэтому изменить их может только root
	def get_owner_organization(self): return None
	def get_owner_user(self): return None



class Building(db.Model, OwnerMixin):
	"""Building model"""
	__tablename__ = 'building'
	id = db.Column(db.Integer, primary_key=True)
	active = db.Column(db.Boolean, default=True, nullable=False)

	city_id = db.Column(db.Integer, db.ForeignKey('city.id'), nullable=True)
	city = db.relationship('City', backref=db.backref('buildings', lazy='dynamic'))

	organization_id = db.Column(db.Integer, db.ForeignKey('organization.id'), nullable=False)
	organization = db.relationship('Organization', backref=db.backref('buildings', lazy='dynamic'))

	user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
	user = db.relationship('User', backref=db.backref('buildings', lazy='dynamic'))

	address = db.Column(db.String(300), default='', nullable=False)
	name = db.Column(db.String(80), default='', nullable=False)
	short_description = db.Column(db.Text, default='', nullable=False)
	# Полигон с границами здания
	geodata = db.Column(db.Text, nullable=True)
	# Точки входа в задние
	entries = db.Column(db.Text, nullable=True)

	# Координаты центра здания
	coords_lat = db.Column(db.Numeric(16, 9, asdecimal=False), default=0.0, nullable=False)
	coords_lng = db.Column(db.Numeric(16, 9, asdecimal=False), default=0.0, nullable=False)

	time_created = db.Column(db.TIMESTAMP(timezone=True), server_default=db.func.now(), nullable=False)
	time_updated = db.Column(db.TIMESTAMP(timezone=True), server_default=db.func.now(), onupdate=db.func.now(), nullable=False)

	def __repr__(self):
		return "<Building {0} {1}>".format(self.id, self.name)

	# --- COORDS dynamic property ---
	@property
	def coords(self): return (self.coords_lng, self.coords_lat)
	@coords.setter
	def coords(self, coords): self.coords_lng, self.coords_lat = coords

	def get_owner_organization(self): return self.organization_id
	def get_owner_user(self): return self.user_id


class Floor(db.Model, OwnerMixin):
	"""Floor model"""
	__tablename__ = 'floor'
	id = db.Column(db.Integer, primary_key=True)
	active = db.Column(db.Boolean, default=True, nullable=False)

	building_id = db.Column(db.ForeignKey('building.id'), nullable=False)
	building = db.relationship('Building', backref=db.backref('floors', lazy='dynamic'))

	name = db.Column(db.String(100), default='', nullable=False)
	position = db.Column(db.Integer(), default=0, nullable=False)

	geodata = db.Column(db.Text())

	time_created = db.Column(db.TIMESTAMP(timezone=True), server_default=db.func.now(), nullable=False)
	time_updated = db.Column(db.TIMESTAMP(timezone=True), server_default=db.func.now(), onupdate=db.func.now(), nullable=False)

	user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
	user = db.relationship('User', backref=db.backref('floors', lazy='dynamic'))

	organization_id = db.Column(db.Integer, db.ForeignKey('organization.id'), nullable=False)
	organization = db.relationship('Organization', backref=db.backref('floors', lazy='dynamic'))

	def __repr__(self):
		return '<Floor {0} {1}>'.format(self.id, self.name)

	def get_owner_organization(self): return self.organization_id
	def get_owner_user(self): return self.user_id


class FloorBitmap(db.Model):
	"""Bitmap model for floor"""
	__tablename__ = 'floor_bitmaps'
	id = db.Column(db.Integer, primary_key=True)
	type = db.Column(db.String(10), default='main', nullable=False)
	name = db.Column(db.String(100), default='', nullable=False)
	hash = db.Column(db.String(32), nullable=False, unique=True)

	url = db.Column(db.String(1000))
	width = db.Column(db.Integer(), default=0.0, nullable=False)
	height = db.Column(db.Integer(), default=0.0, nullable=False)
	scale = db.Column(db.Float(asdecimal=False), default=0.0, nullable=False)

	floor_id = db.Column(db.ForeignKey('floor.id'), nullable=False)
	floor = db.relationship('Floor', backref=db.backref('bitmaps', lazy='dynamic'))

	def get_owner_organization(self): return self.floor.organization_id
	def get_owner_user(self): return self.floor.user_id

	def make_hash(self, img_buffer):
		hasher = hashlib.md5
		result = None
		if not img_buffer:
			with open(self.url, 'rb') as fp:
				img_buffer = fp.read()
		hasher.update(img_buffer)
		self.hash = hasher.hexdigest()


"""
{
  "id": 1,
  "active": 1,
  "city_id": 2, // Набережные челны
  "organization_id": 2 // Рынок алан - организация, владеющая зданием
  "address": "Ул. Пушкина, дом колотушкина",
  "name": "Рынок Алан",
  "short_description": "Носки, трусы и прочий ширпотреб", // Короткое описание.
  "coords": [55.7, 52.41] // Координаты центра здания.
  "geodata": {
    "type": "Feature",
    "geometry": {
      "type": "Polygon",
      "coordinates": [[
        [55.765661, 52.410835], [55.764882, 52.412112], [55.765849, 52.413905],
        [55.766239, 52.413250], [55.765708, 52.412239], [55.766081, 52.411614],
        [55.765661, 52.410835]
      ]]
    }
  }, // GeoJSON - строка с полигонами.
  "entries": [
        {
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": [55.766239, 52.413250]
          },
        },
        {...}
  ],
  
  "geodata": [
        [55.765661, 52.410835], [55.764882, 52.412112], [55.765849, 52.413905],
        [55.766239, 52.413250], [55.765708, 52.412239], [55.766081, 52.411614],
        [55.765661, 52.410835], ...
  ],
  "entries": [
    [55.766239, 52.413250],
    [55.766239, 52.413250],
    ...
  ],
  "time_updated": "2018-05-31 05:34:20", // Время последнего изменения здания.
}
"""
