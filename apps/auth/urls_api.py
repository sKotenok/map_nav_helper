from flask import request, jsonify, redirect, url_for, abort
from flask_jwt_extended import (
	create_access_token,
	create_refresh_token,
	jwt_refresh_token_required,
	get_jwt_identity,
	set_access_cookies,
	set_refresh_cookies,
	unset_jwt_cookies,
	jwt_required,
	jwt_optional,
	get_current_user,
	get_raw_jwt
)

from models.auth import User, Role
from extensions import db, pwd_context, jwt
from common.model_resource import REMOTE_HEADERS




def on_expired_token():
	json_data = request.get_json()
	if json_data:
		# Let's pretend it was an api query. In this case we need to say that he need to use refresh-token page
		return jsonify({
			'error': 'Access token is expired, go to refresh page to get new access token',
			'refresh_page': url_for('auth.refresh')
		})
	else:
		# It was just a normal user. Let's remove access token and redirect him to a login page.
		resp = redirect(url_for('auth.login'))
		unset_jwt_cookies(resp)
		return resp, 200, REMOTE_HEADERS

jwt.expired_token_loader(on_expired_token)





def api_urls(api, blueprint):

	@blueprint.route('/auth/login', methods=['GET', 'POST', 'OPTIONS'])
	@jwt_optional
	def login_api():
		if request.method == 'OPTIONS':
			print('Request method is OPTIONS')
			return 'Ok', 200, REMOTE_HEADERS

		# raw_jwt = get_raw_jwt()
		identity = get_jwt_identity()
		if identity:
			return jsonify({
				'user_id': identity,
				'access_token': create_access_token(identity=identity),
				'refresh_token': create_refresh_token(identity=identity)
			}), 200, REMOTE_HEADERS
		if request.method == 'GET':
			# TODO Временно разрешаю логиниться через GET для тестов
			device_token = request.values.get('device_token', None)
			device_model = request.values.get('device_model', None)
			push_token = request.values.get('push_token', None)
			username = request.values.get('username', None)
			password = request.values.get('password', None)
		else:
			if not request.is_json:
				print('Missing JSON in request')
				return jsonify({'error': "Missing JSON in request"}), 400, REMOTE_HEADERS
			device_token = request.json.get('device_token', None)
			device_model = request.json.get('device_model', None)
			push_token = request.json.get('push_token', None)
			username = request.json.get('username', None)
			password = request.json.get('password', None)

		user = None
		if username and password:
			user = User.query.filter_by(name=username).first()
			if user is None or not pwd_context.verify(password, user.password):
				return jsonify({'error': "Incorrect login or password"}), 400, REMOTE_HEADERS
		elif device_token:
			user = User.query.filter_by(device_token=device_token).first()
		else:
			print('Missing device_token or push_token')
			return jsonify({'error': "Missing device_token or push_token"}), 400, REMOTE_HEADERS

		if user is None:
			# There is no user with such device_token.
			role_user = Role.query.filter_by(id=2).first()
			user = User( device_token=device_token, device_model=device_model, push_token=push_token, login=username, roles=[role_user] )
			db.session.add(user)
			db.session.commit()

		if user.push_token != push_token:
			user.push_token = push_token
			db.session.commit()

		access_token = create_access_token(identity=user.id)
		refresh_token = create_refresh_token(identity=user.id)

		ret = {
			'user_id': user.id,
			'access_token': access_token,
			'refresh_token': refresh_token
		}
		return jsonify(ret), 200, REMOTE_HEADERS


	@blueprint.route('/auth/refresh', methods=['GET', 'POST'])
	@jwt_refresh_token_required
	def refresh():
		current_user = get_jwt_identity() # current_user == current_user_id
		access_token = create_access_token(identity=current_user)
		ret = {
			'access_token': access_token
		}
		resp = jsonify(ret)
		set_access_cookies(resp, access_token)

		return resp, 200, REMOTE_HEADERS



