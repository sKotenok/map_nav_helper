from extensions import db


class OwnerMixin:

	@classmethod
	def get_owner_user_field(cls):
		model_fields = ('user', 'owner', 'creator', 'author')
		id_fields = ('user_id', 'owner_id', 'creator_id', 'author_id')

		for f in id_fields:
			if hasattr(cls, f):
				return getattr(cls, f)
		for f in model_fields:
			if hasattr(cls, f):
				return getattr(cls, f).id
		return None

	@classmethod
	def get_owner_organization_field(cls):
		if hasattr(cls, 'organization'):
			return cls.organization.id
		elif hasattr(cls, 'organization_id'):
			return cls.organization_id
		return None

	def get_owner_user_id(self):
		"""Get user that own this field."""
		model_fields = ('user', 'owner', 'creator', 'author')
		id_fields = ('user_id', 'owner_id', 'creator_id', 'author_id')

		for f in id_fields:
			if hasattr(self, f):
				return getattr(self, f)
		for f in model_fields:
			if hasattr(self, f):
				return getattr(self, f).id
		return None

	def get_owner_organization_id(self):
		"""Get organization that own this field"""
		if hasattr(self, 'organization'):
			return self.organization.id
		elif hasattr(self, 'organization_id'):
			return self.organization_id
		return None
