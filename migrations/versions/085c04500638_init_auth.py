"""Init Auth

Revision ID: 085c04500638
Revises:
Create Date: 2018-09-13 15:55:59

python3 manage.py db revision --autogenerate -m "Init auth"
"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '085c04500638'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
	# Authorization tables
	op.create_table('organization',
		sa.Column('id', sa.Integer(), nullable=False),
		sa.Column('name', sa.String(length=100), nullable=False),
		sa.Column('parent_id', sa.Integer(), nullable=True),
		sa.ForeignKeyConstraint(('parent_id',), ['organization.id'], ),
		sa.PrimaryKeyConstraint('id')
	)

	op.create_table('user',
		sa.Column('id', sa.Integer(), nullable=False),
		sa.Column('device_token', sa.String(length=100), nullable=True, server_default=''),
		sa.Column('device_model', sa.String(length=200), nullable=False, server_default=''),
		sa.Column('push_token', sa.String(length=500), nullable=True, server_default=''),
		sa.Column('login', sa.String(length=80), nullable=True),
		sa.Column('name', sa.String(length=200), nullable=True),
		sa.Column('phone', sa.String(length=20), nullable=True),
		sa.Column('email', sa.String(length=80), nullable=True),
		sa.Column('password', sa.String(length=255), nullable=True),
		sa.Column('active', sa.Boolean(), nullable=False, server_default=sa.literal(True)),
		sa.Column('organization_id', sa.Integer(), nullable=True),

		sa.ForeignKeyConstraint(('organization_id',), ['organization.id'], ),
		sa.PrimaryKeyConstraint('id'),
		sa.UniqueConstraint('device_token'),
		sa.UniqueConstraint('email'),
		sa.UniqueConstraint('login'),
		sa.UniqueConstraint('push_token')
	)

	op.create_table('permission',
		sa.Column('id', sa.Integer(), nullable=False),
		sa.Column('module', sa.String(length=30), nullable=False, server_default=''),
		sa.Column('name', sa.String(length=100), nullable=False),
		sa.PrimaryKeyConstraint('id')
	)

	op.create_table('role',
		sa.Column('id', sa.Integer(), nullable=False),
		sa.Column('name', sa.String(length=100), nullable=False, server_default=''),
		sa.Column('organization_id', sa.Integer(), nullable=True),

		sa.ForeignKeyConstraint(('organization_id',), ['organization.id'], ),
		sa.PrimaryKeyConstraint('id')
	)

	op.create_table('role_permission',
		sa.Column('permission_id', sa.Integer(), nullable=False),
		sa.Column('role_id', sa.Integer(), nullable=False),
		sa.ForeignKeyConstraint(('permission_id',), ['permission.id'], ),
		sa.ForeignKeyConstraint(('role_id',), ['role.id'], ),
		sa.PrimaryKeyConstraint('permission_id', 'role_id')
	)

	op.create_table('user_role',
		sa.Column('user_id', sa.Integer(), nullable=False),
		sa.Column('role_id', sa.Integer(), nullable=False),
		sa.ForeignKeyConstraint(('role_id',), ['role.id'], ),
		sa.ForeignKeyConstraint(('user_id',), ['user.id'], ),
		sa.PrimaryKeyConstraint('user_id', 'role_id')
	)


def downgrade():
	op.drop_table('user_role')
	op.drop_table('user_group')
	op.drop_table('role_permission')
	op.drop_table('user')
	op.drop_table('role')
	op.drop_table('group')
	op.drop_table('organization')
	op.drop_table('permission')
