from .app import create_wsgi_app


app = create_wsgi_app()
