"""
Default configuration

Use env var to override
"""
import datetime

DEBUG = True
SECRET_KEY = "SECRET KEY"

# SQLALCHEMY_DATABASE_URI = "sqlite:////home/skotenok/Job/SotaPay/map_nav_api_flask/api.db"
SQLALCHEMY_DATABASE_URI = "mysql+mysqlconnector://root:root00@localhost/map_nav_helper"
SQLALCHEMY_TRACK_MODIFICATIONS = False

SQLALCHEMY_BINDS = {
	'old_db': 'mysql://root:root00@localhost/old_map_nav_helper',
}

if DEBUG:
	SQLALCHEMY_ECHO = True
	#SQLALCHEMY_ECHO = False

	KONCH_SHELL='ipy'


# Flask-jwt-extended
JWT_TOKEN_LOCATION = ['cookies', 'headers']
JWT_COOKIE_CSRF_PROTECT = False
JWT_ACCESS_TOKEN_EXPIRES=datetime.timedelta(days=1)


# Flask-pushjack
APNS_CERTIFICATE = '<path/to/certificate.pem>'
FCM_API_KEY = 'AAAA8RV2rlo:APA91bGeXIw2D71qYfuwUwSw1vDg5CKfeEfgtZr-dyLyhHeQUqpMofk4vqo-02-jDWaravHmJXsZtJRS6LTxviCootwn-Xqu972FyhJJnvsYtYGbFCAeO-PJrjWg7JD_jn_pHihzPIeJv7Fwue30lnqjpx-YGWSNaQ'


# My
STATIC_DIR = 'static'
FILES_BITMAP_DIR = 'BITMAPS'
FILES_IMAGE_DIR = 'IMAGES'
FILES_OTHER_DIR = 'OTHER'
FILES_USER_MESSAGES_DIR = 'USER_MESSAGE_IMAGES'

