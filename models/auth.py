from extensions import db, pwd_context
from sqlalchemy.ext.declarative import declarative_base



def is_user_root(user):
	return user.organization_id == 1


def create_user(user_params=None, organization=None, groups=None):

	user = User()
	if organization:
		User.organization = organization

	if user_params:
		for prop, value in user_params.items():
			User.__dict__[prop] = value
	db.session.add(User)
	return User


def get_user_roles(user=None, user_id=None):
	"""Returns user role if user exists. Temporary - user has string 'role' field, not m-m tables"""
	roles = []
	if user:
		roles = user.roles.all()
	elif user_id:
		user = User.query.get_or_404(user_id)
		roles = user.roles.all()
	return roles


class Organization(db.Model):
	"""Organization model"""
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100), nullable=False)

	parent_id = db.Column(db.Integer, db.ForeignKey('organization.id'), nullable=True)
	parent = db.relation('Organization', remote_side=(id,), backref=db.backref('childs', lazy='dynamic'))



class User(db.Model):
	"""Basic user model"""
	__tablename__ = 'user'
	id = db.Column(db.Integer, primary_key=True)
	active = db.Column(db.Boolean, default=True, nullable=False)

	device_token = db.Column(db.String(100), nullable=True, unique=True)
	device_model = db.Column(db.String(200), nullable=False, default='')
	push_token = db.Column(db.String(500), nullable=True, unique=True)
	phone = db.Column(db.String(20))

	login = db.Column(db.String(80), unique=True, nullable=True)
	name = db.Column(db.String(200), default='')
	email = db.Column(db.String(80), unique=True, nullable=True)
	password = db.Column(db.String(255), nullable=True)

	organization_id = db.Column(db.Integer, db.ForeignKey('organization.id'), nullable=True)
	organization = db.relationship('Organization', backref='users')

#	groups = db.relationship('Group', secondary='user_group', back_populates='users', lazy='dynamic')
	roles = db.relationship('Role', secondary='user_role', back_populates='users', lazy='dynamic')

	def __init__(self, **kwargs):
		super(User, self).__init__(**kwargs)
		if (self.password):
			self.password = pwd_context.hash(self.password)

	def __repr__(self):
		return "<User %s>" % self.name

	def set_password(self, password):
		self.password = pwd_context.hash(password)












# class Group(db.Model):
# 	"""Groups for RBAC"""
# 	id = db.Column(db.Integer, primary_key=True)
# 	name = db.Column(db.String(80), default='', nullable=False)
#
# 	user_id = db.Column(db.Integer, nullable=False)
#
# 	time_created = db.Column(db.TIMESTAMP(timezone=True), server_default=db.func.now(), nullable=False)
#
# 	users = db.relationship('User', secondary='user_group', back_populates='groups', lazy='dynamic')
# #roles = db.relationship('Role', secondary=group_role_table, back_populates='groups')
#
#
# class UserGroup(db.Model):
# 	__tablename__ = 'user_group'
# 	id = db.Column(db.Integer, primary_key=True)
# 	user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
# 	user = db.relationship('User', backref=db.backref('user_groups', lazy='dynamic'))
#
# 	group_id = db.Column(db.Integer, db.ForeignKey('group.id'), nullable=False)
# 	group = db.relationship('Group', backref=db.backref('users_groups', lazy='dynamic'))
#
# 	accepted = db.Column(db.Boolean, default=False, nullable=False)
# 	group_owner = db.Column(db.Boolean, default=False, nullable=False)


# user_group_table = db.Table('user_group', db.Model.metadata,
# 	db.Column('user_id', db.Integer, db.ForeignKey('user.id'), nullable=False),
# 	db.Column('group_id', db.Integer, db.ForeignKey('group.id'), nullable=False)
# )

# group_role_table = db.Table('group_role', db.Model.metadata,
# 	db.Column('group_id', db.Integer, db.ForeignKey('group.id'), nullable=False),
# 	db.Column('role_id', db.Integer, db.ForeignKey('role.id'), nullable=False)
# )


# Конкретная роль пользователя в организации - назначается админом компании.
class Role(db.Model):
	"""Simple role model for RBAC"""
	__tablename__ = 'role'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100), nullable=False, default='')

	organization_id = db.Column(db.Integer, db.ForeignKey('organization.id'), nullable=True)
	organization = db.relationship('Organization', backref='roles')

	users =  db.relationship('User', secondary='user_role', back_populates='roles', lazy='dynamic')


class Permission(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	module = db.Column(db.String(30), nullable=False, default='')
	name = db.Column(db.String(100), nullable=False)



class UserRole(db.Model):
	__tablename__ = 'user_role'
	user_id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
	role_id = db.Column(db.Integer, db.ForeignKey('role.id'), primary_key=True)


class RolePermission(db.Model):
	__tablename__ = 'role_permission'
	permission_id = db.Column(db.Integer, db.ForeignKey('permission.id'), primary_key=True)
	role_id = db.Column(db.Integer, db.ForeignKey('role.id'), primary_key=True)


