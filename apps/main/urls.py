from flask import request, jsonify, Blueprint, render_template, redirect, url_for, abort
from flask_jwt_extended import get_current_user, jwt_optional

#from models.auth import User
#from extensions import db, pwd_context, jwt


blueprint = Blueprint('main', __name__, url_prefix='/')


@jwt_optional
@blueprint.route('/index.html', methods=['GET'])
@blueprint.route('/', methods=['GET'])
def index():
	user = get_current_user()
	return render_template('main/index.html', user=user)

