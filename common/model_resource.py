import pytz
from collections.abc import Sequence
# from sqlalchemy import func
from flask import request, make_response
from flask_restful import Resource
from flask_jwt_extended import jwt_required, jwt_optional, get_current_user

from extensions import ma, db, jwt
from common.pagination import paginate, paginate_one
from models.auth import User
from common.file_utils import upload_file_json, upload_file_post


REMOTE_HEADERS = {
	'Access-Control-Allow-Origin': '*',
	'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
	'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token, Authorization',
}


# TODO: Перенести в глобальные файлы, т.к. метод нужен практически в любом запросе.
@jwt.user_loader_callback_loader
def user_loader(jwt_identity):
	print('jwt_identity:', jwt_identity)
	if jwt_identity:
		user = User.query.filter_by(id=int(jwt_identity)).first()
		return user
	return None


class ModelResourceDetail(Resource):

	model = None
	schema = None
	permission = None
	method_decorators = [jwt_required]

	def options(self, *args, **kwargs):
		return {}, 200, REMOTE_HEADERS

	def head(self, model_id):
		if hasattr(self.model, 'time_updated'):
			model_instance = self.model.query.get_or_404(model_id)
			lm_t = model_instance.time_updated.replace(tzinfo=pytz.UTC).strftime('%a, %d %b %Y %I:%M:%S %Z')
			return {}, 200, { 'Last-Modified': lm_t, **REMOTE_HEADERS}
		else:
			return {}, 200, { 'Last-Modified': '', **REMOTE_HEADERS }

	def get(self, model_id):
		user = get_current_user()
		if self.permission:
			permission = self.permission(user, self.model)
			if not permission.can_read_table():
				return {'error': 'You don\'t have permission to view this entry'}, 403, REMOTE_HEADERS
			model_instance, schema_instance  = self._get(model_id)
			if not permission.can_read(model_instance):
				return {'error': 'You don\'t have permission to view this entry'}, 403, REMOTE_HEADERS
		else:
			model_instance, schema_instance  = self._get(model_id)
		return paginate_one(model_instance, schema_instance), 200, REMOTE_HEADERS

	def _get(self, model_id):
		schema_instance = self.schema()
		model_instance = self.model.query.get_or_404(model_id)
		return model_instance, schema_instance


	def put(self, model_id):
		user = get_current_user()
		if self.permission:
			permission = self.permission(user, self.model)
			if not permission.can_update(model_id):
				return {'error': 'You don\'t have permission to edit this entry'}, 403, REMOTE_HEADERS
		result, errors = self._put(model_id, request.get_json(), user)
		if errors:
			return errors, 422, REMOTE_HEADERS
		print('model_instance was updated', result)
		return { "results": True }, 200, REMOTE_HEADERS

	def _put(self, model_id, json_data, user=None, user_id_field='user_id'):
		schema_instance = self.schema(partial=True)
		model_instance = self.model.query.get_or_404(model_id)
		if user and user_id_field in json_data:
			json_data[user_id_field] = user.id
		model_instance, errors = schema_instance.load(json_data, instance=model_instance)
		if not errors:
			db.session.commit()
		return model_instance, errors


	def delete(self, model_id):
		user = get_current_user()
		if self.permission:
			permission = self.permission(user, self.model)
			if not permission.can_delete(model_id):
				return {'error': 'You don\'t have permission to delete this entry'}, 403
		deleted_counts = self._delete(model_id)
		if not deleted_counts:
			return { 'error': 'There was no row with id {0} to delete'.format(model_id)}
		return { 'results': True }, 200, REMOTE_HEADERS

	def _delete(self, model_id):
		model_instance = self.model.query.get_or_404(model_id)
		deleted_count = db.session.delete(model_instance)
		if deleted_count:
			db.session.commit()
		return deleted_count


class ModelResourceList(Resource):

	model = None
	schema = None
	filtering = None
	permission = None
	method_decorators = [jwt_required]


	def options(self, *args, **kwargs):
		return {}, 200, REMOTE_HEADERS

	def head(self):
		if hasattr(self.model, 'time_updated'):
			max_time = db.session.query(db.func.max(self.model.time_updated)).scalar()
			lm_t = max_time.replace(tzinfo=pytz.UTC).strftime('%a, %d %b %Y %I:%M:%S %Z')
			return {}, 200, { 'Last-Modified': lm_t, **REMOTE_HEADERS }
		else:
			return {}, 200, { 'Last-Modified': '', **REMOTE_HEADERS }


	def get(self):
		user = get_current_user()
		if self.permission:
			permission = self.permission(user, self.model)
			if not permission.can_read_table():
				return {'error': 'You don\'t have permission to get this entry'}, 403, REMOTE_HEADERS
			query, schema_instance = self._get()
			query = permission.filter_for_read(query)
		else:
			query, schema_instance = self._get()

		if hasattr(self.model, 'active'):
			query = query.filter_by(active=1)
		return paginate(query, schema_instance), 200, REMOTE_HEADERS

	def _get(self, query=None):
		if not query:
			query = self.model.query
		schema_instance = self.schema(many=True)
		if self.filtering:
			if isinstance(self.filtering, Sequence):
				for f in self.filtering:
					query = f(self, query, self.model, request)
			else:
				query = self.filtering(query, self.model, request)
		return query, schema_instance


	def post(self):
		if not request.json:
			print('Request.headers', request.headers)
			print('Request.data', request.get_data())
			return {'error': 'Not a json object was send'}

		user = get_current_user()
		if self.permission:
			permission = self.permission(user, self.model)
			if not permission.can_add_table():
				return {'error': 'You don\'t have permission to add this entry'}, 403, REMOTE_HEADERS
			model_instance, errors = self._post(request.get_json(), user)
			if not permission.can_add(model_instance):
				return {'error': 'You don\'t have permission to add this entry'}, 403, REMOTE_HEADERS
		else:
			model_instance, errors = self._post(request.get_json(), user)

		if errors:
			return errors, 422, REMOTE_HEADERS
		print('Model instance was added')
		return { 'results': True, 'id': model_instance.id }, 200, REMOTE_HEADERS

	def _post(self, json_data, user=None, user_field='user_id'):
		schema_instance = self.schema()
		if user and user_field in json_data:
			json_data[user_field] = user.id
		model_instance, errors = schema_instance.load(json_data)
		if not errors:
			db.session.add(model_instance)
			db.session.commit()
		return model_instance, errors




class ModelResourceMany(Resource):
	model = None
	schema = None
	permission = None
	method_decorators = [jwt_required]


	def options(self, *args, **kwargs):
		return {}, 200, REMOTE_HEADERS

	def post(self):
		if request.json:
			print('Request.json:', request.get_json())
		else:
			print('Request.headers', request.headers)
			print('Request.data', request.get_data())
			return {'error': 'Not a json object was send'}

		user = get_current_user()
		if self.permission:
			permission = self.permission(user, self.model)
			if not permission.can_add():
				return {'error': 'You don\'t have permission to add this entry'}, 403, REMOTE_HEADERS

		model_instances, errors = self._post(request.get_json(), user)
		if errors:
			return errors, 422, REMOTE_HEADERS

		print('Model instances were added')
		result = [ m.id for m in model_instances ]

		return { 'results': result }, 200, REMOTE_HEADERS

	def _post(self, json_data, user=None, user_field='user_id'):
		schema_instance = self.schema(many=True)
		if len(json_data) and user_field in json_data[0]:
			for json_instance in json_data:
				json_instance[user_field] = user.id
		model_instances, errors = schema_instance.load(json_data)

		if not errors:
			db.session.bulk_save_objects(model_instances)
			db.session.commit()
		return model_instances, errors




class ModelResourceListByParent(Resource):
	base_model = None
	relations = { 'parent_id': None, 'childs': None }
	child_schema = None
	child_model = None
	filtering = None
	permission = None
	method_decorators = [jwt_required]


	def options(self, *args, **kwargs):
		return {}, 200, REMOTE_HEADERS

	def head(self, model_id):
		if (self.child_model and hasattr(self.child_model, 'time_updated') and 'parent_id' in self.relations and self.relations['parent_id']):
			max_time = db.session.query(db.func.max(self.child_model.time_updated)).filter_by(**{self.relations['parent_id']: int(model_id)}).scalar()
			lm_t = max_time.replace(tzinfo=pytz.UTC).strftime('%a, %d %b %Y %I:%M:%S %Z')
			return {}, 200, { 'Last-Modified': lm_t, **REMOTE_HEADERS }
		else:
			return {}, 200, { 'Last-Modified': '', **REMOTE_HEADERS }


	def get(self, model_id):
		user = get_current_user()
		if self.permission:
			permission = self.permission(user, self.child_model)
			if not permission.can_read_table():
				return {'error': 'You don\'t have permission to view this entry'}, 403, REMOTE_HEADERS
			query, schema_instance = self._get(model_id, active_only=True)
			query = permission.filter_for_read(query)
		else:
			query, schema_instance = self._get(model_id, active_only=True)
		return paginate(query, schema_instance), 200, REMOTE_HEADERS

	def _get(self, base_model_id, active_only=False):
		schema_instance = self.child_schema(many=True)
		base_model_instance = self.base_model.query.get_or_404(base_model_id)
		if self.relations['childs']:
			query = getattr(base_model_instance, str(self.relations['childs']))
		else:
			raise Exception('relations.childs can not be None!')

		if active_only and hasattr(self.child_model, 'active'):
			query = query.filter_by(active=1)
		if self.filtering:
			if isinstance(self.filtering, Sequence):
				for f in self.filtering:
					query = f(self, query, self.child_model, request, parent_id=base_model_instance.id)
			else:
				query = self.filtering(query, self.child_model, request, parent_id=base_model_instance.id)

		return query, schema_instance



class ModelFile(Resource):
	file_field = 'file'
	user_field = 'user_id'
	path_to_save = 'uploads'
	model = None
#	schema = None
	permission = None
	before_upload = []
	after_upload = []
	method_decorators = [jwt_optional]

	def __init__(self, *args, **kwargs):
		super(ModelFile, self).__init__(*args, **kwargs)
		self._buffer = {}


	def options(self, *args, **kwargs):
		return {}, 200, REMOTE_HEADERS

	def get(self):
		# Скажет, загружена ли данная картинка
		return {'error': 'You don\'t have permission to see this entry'}, 403, REMOTE_HEADERS

	def post(self):
		# Загрузить файл POST-запросом
		user = get_current_user()

		print('HEADERS:', request.headers)
		print('DATA:', request.data)
		if not user:
			return {'error': 'Not authorized'}, 403, REMOTE_HEADERS

		if self.permission:
			permission = self.permission(user, self.model)
			if not permission.can_add_table():
				return {'error': 'You don\'t have permission to add this entry'}, 403, REMOTE_HEADERS

		if request.json:
			file_name, file_path, file_url = upload_file_json(
				request.get_json(),
				file_field=self.file_field,
				path_to_save=self.path_to_save
			)
		else:
			file_name, file_path, file_url = upload_file_post(
				request.files,
				file_field=self.file_field,
				path_to_save=self.path_to_save
			)

		if not (file_name and file_path):
			return {'error': 'You don\'t have permission to add this entry'}, 403, REMOTE_HEADERS

		if self.after_upload:
			for func in self.after_upload:
				func_result = func(self, file_name, file_path, file_url)
				if func_result: self._buffer.update(func_result)

		# if self.model and request.json:
		# 	json_data = request.get_json()
		# 	json_data[self.file_field] = file_name
		#
		# 	schema_instance = self.schema()
		# 	if user and self.user_field in json_data:
		# 		json_data[self.user_field] = user.id
		#
		# 	model_instance, errors = schema_instance.load(json_data)
		# 	if not errors:
		# 		db.session.add(model_instance)
		# 		db.session.commit()

			print('Model instances were added', self._buffer)
		return {'results': 'Ok', 'url': file_url, **self._buffer }, 200, REMOTE_HEADERS







# class ModelResourceListByManyToManyRelated(Resource):
# 	base_model = None
# 	relations = { 'parent_id': None, 'childs': None }
# 	child_schema = None
# 	child_model = None
# 	filtering = None
# 	method_decorators = [jwt_required]
#
# 	def get(self, model_id):
# 		user = get_current_user()
# 		if not Permission(user, self.child_model).can('Read'):
# 			return {'error': 'You don\'t have permission for this entry'}, 403
# 		query, schema_instance = self._get(model_id, active_only=True)
# 		return paginate(query, schema_instance), 200, REMOTE_HEADERS
#
# 	def _get(self, base_model_id, active_only=False):
# 		schema_instance = self.child_schema(many=True)
# 		base_model_instance = self.base_model.query.get_or_404(base_model_id)
# 		if self.relations['childs']:
# 			query = getattr(base_model_instance, str(self.relations['childs']))
# 		else:
# 			raise Exception('relations.childs can not be None!')
#
# 		if active_only and hasattr(self.child_model, 'active'):
# 			query = query.filter_by(active=1)
# 		if self.filtering:
# 			if isinstance(self.filtering, Sequence):
# 				for f in self.filtering:
# 					query = f(self, query, self.child_model, request, parent_id=base_model_instance.id)
# 			else:
# 				query = self.filtering(query, self.child_model, request, parent_id=base_model_instance.id)
#
# 		return query, schema_instance



