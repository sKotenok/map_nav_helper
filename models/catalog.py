from extensions import db
from common.base_model import OwnerMixin


class Shop(db.Model, OwnerMixin):
	"""GeoObject model"""
	__tablename__ = 'geo_object_type'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100), nullable=False, default='')
	address = db.Column(db.String(300), default='', nullable=False)
	short_description = db.Column(db.Text, default='', nullable=False)
	active = db.Column(db.Boolean, default=True, nullable=False)

	floor_id = db.Column(db.Integer, db.ForeignKey('floor.id'), nullable=False)
	floor = db.relationship('Floor', backref=db.backref('layers', lazy='dynamic'))

	building_id = db.Column(db.Integer, db.ForeignKey('building.id'), nullable=True)
	building = db.relationship('Building', backref=db.backref('control_points', lazy='dynamic'))

	# Ссылка на связанный с магазином гео-обьект.
	geo_object_id = db.Column(db.Integer, db.ForeignKey('geo_object.id'), nullable=False)
	geo_object = db.relationship('GeoObject', backref=db.backref('shops', lazy='dynamic'))

	# Компания - владелец магазина.
	organization_id = db.Column(db.Integer, db.ForeignKey('organization.id'), nullable=True)
	organization = db.relationship('Organization', backref=db.backref('geo_objects', lazy='dynamic'))

	# Пользователь - владелец магазина.
	user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
	owner = db.relationship('Organization', backref=db.backref('shops', lazy='dynamic'))

	# Координаты центра магазина
	coords_lat = db.Column(db.Numeric(16, 9, asdecimal=False), default=0.0, nullable=False)
	coords_lng = db.Column(db.Numeric(16, 9, asdecimal=False), default=0.0, nullable=False)

	time_created = db.Column(db.TIMESTAMP(timezone=True), server_default=db.func.now(), nullable=False)
	time_updated = db.Column(db.TIMESTAMP(timezone=True), server_default=db.func.now(), onupdate=db.func.now(), nullable=False)

	def __repr__(self):
		return "<Shop %d %s>" % (self.id, self.name)

	# --- COORDS dynamic property ---
	@property
	def coords(self): return self.coords_lng, self.coords_lat
	@coords.setter
	def coords(self, coords): self.coords_lng, self.coords.lat = coords

	# Данные о владельце - нужны для работы прав доступа.
	def get_owner_organization(self): return self.organization_id
	def get_owner_user(self): return self.user_id


class Catalog(db.Model, OwnerMixin):
	__tablename__ = 'catalog'
	id = db.Column(db.Integer, primary_key=True)

	parent_id = db.Column(db.Integer, db.ForeignKey('catalog.id'), nullable=True)
	parent = db.relation('Catalog', remote_side=(id,), backref=db.backref('childs', lazy='dynamic'))

	catalog_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
	catalog = db.relationship('Organization', backref=db.backref('shops', lazy='dynamic'))

	name = db.Column(db.String(100), nullable=False, default='')
	short_description = db.Column(db.Text, default='', nullable=False)

	# У общих справочников нет владельца, поэтому изменить их может только root
	def get_owner_organization(self): return None
	def get_owner_user(self): return None


class ProductProperty(db.Model, OwnerMixin):
	__tablename__ = 'product_property'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100), nullable=False, default='')
	short_description = db.Column(db.Text, default='', nullable=False)
	default_value = db.Column(db.Text, nullable=False, default='')
	type = db.Column(db.String(20), default='float', nullable=False)

	# У общих справочников нет владельца, поэтому изменить их может только root
	def get_owner_organization(self): return None
	def get_owner_user(self): return None



class Product(db.Model, OwnerMixin):
	__tablename__ = 'product'
	id = db.Column(db.Integer, primary_key=True)

	# Магазин, к которому привязан данный Товар.
	shop_id = db.Column(db.Integer, db.ForeignKey('shop.id'), nullable=True)
	shop = db.relationship('Organization', backref=db.backref('products', lazy='dynamic'))

	# Элемент каталога, к которому привязан данный Товар.
	catalog_id = db.Column(db.Integer, db.ForeignKey('catalog.id'), nullable=True)
	catalog = db.relationship('Organization', backref=db.backref('shops', lazy='dynamic'))

	name = db.Column(db.String(100), nullable=False, default='')
	price = db.Column(db.Float, nullable=False, default=0.0)

	short_description = db.Column(db.Text, default='', nullable=False)
	description = db.Column(db.Text, default='', nullable=False)

	# Данные о владельце - нужны для работы прав доступа.
	def get_owner_organization(self): return self.shop.organization_id
	def get_owner_user(self): return self.shop.user_id


class ProductPropertyValue(db.Model, OwnerMixin):
	__tablename__ = 'product_property_value'
	id = db.Column(db.Integer, primary_key=True)
	value = db.Column(db.Text(), default='0.0', nullable=False)

	product_id = db.Column(db.Integer, db.ForeignKey('product.id'), nullable=False)
	product = db.relationship('Product', backref=db.backref('property_values', lazy='dynamic'))

	property_id = db.Column(db.Integer, db.ForeignKey('product_property.id'), nullable=False)
	property = db.relationship('ProductProperty', backref=db.backref('product_values', lazy='dynamic'))

	def get_owner_organization(self): return self.product.get_owner_organization()
	def get_owner_user(self): return self.product.get_owner_user()


class ProductImage(db.Model, OwnerMixin):
	__tablename__ = 'product_image'
	id = db.Column(db.Integer, primary_key=True)
	active = db.Column(db.Boolean, nullable=False, default=True)
	real_path = db.Column(db.String(1000), nullable=False, default='')
	url = db.Column(db.String(1000), nullable=False, default='')

	product_id = db.Column(db.Integer, db.ForeignKey('product.id'), nullable=False)
	product = db.relationship('Product', backref=db.backref('images', lazy='dynamic'))

	def get_owner_organization(self): return self.product.get_owner_organization()
	def get_owner_user(self): return self.product.get_owner_user()

